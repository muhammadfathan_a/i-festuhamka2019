<?php require("custom/08_contact.html"); ?>
<div class="content" id="contact">
    <div class="btn-close-modal" data-close="modal">
        <span class="icon ma-cross"></span>
    </div>
    <div class="container-contact">
        <div class="contact-caption">
            <div class="contact-content">
                <h2 class="c-white" data-aos="fade-in" data-aos-duration="1000">Our Contact :</h2>
                <p data-aos="fade-right" data-aos-duration="1500">
                    <span class="icon ma-map"></span>&nbsp; 
                    Jl. Tanah Merdeka No. 06, Kp. Rambutan, Pasar Rebo, Jakarta Timur 
                </p>
                <p data-aos="fade-right" data-aos-duration="2000">
                    <span class="icon ma-phone"></span>&nbsp; 
                    0817-7522-3917 - <b> Andika </b>
                </p>
                <p data-aos="fade-right" data-aos-duration="2000">
                    <span class="icon ma-phone"></span>&nbsp; 
                    0898-0613-227 - <b> Yudi </b>
                </p>
                <p data-aos="fade-right" data-aos-duration="2500">
                    <span class="icon ma-envelope"></span>&nbsp; 
                    himatiuhamka31@gmail.com
                </p>
                <p data-aos="fade-right" data-aos-duration="3000">
                    <span class="icon ma-internet"></span>&nbsp; 
                    himatift.uhamka.ac.id
                </p>
            </div>
            <br>
        </div>
        <div class="flat-browser-container" data-aos="fade-in" data-aos-duration="1500">
            <div class="flat-browser">
                <div class="flat-header">
                    <div class="flat-menu">
                        <span class="icon ma-menu"></span>
                    </div>
                    <div class="flat-caption">
                        <div class="caption"> Maps </div>
                    </div>
                </div>
                <div class="flat-body">
                    <div id="maps"></div>
                </div>
            </div>
        </div>
    </div>
</div>