<div class="content" id="footer">
    <div class="img-place">
        <img src="assets/landing/img/Logo.png">
    </div>
    <div class="footer-caption">
        <h1 class="c-black">Thank you!</h1>
        <p>Anyone problem or Question? We can help you if need, please <a href="mailto:info@himatiftuhamka">Message us</a></p>
    </div>
    <div class="social-media-container clearfix">
        <div class="row">
            <div class="cell-xs12 cell-md6 cell-td4 cell-tdl3">
                <a href="javascript:void(0)" class="social-media-button ma-cfacebook" id="facebook">
                    <span class="icon ma-facebook"></span>
                </a>
            </div>
            <div class="cell-xs12 cell-md6 cell-td4 cell-tdl3">
                <a href="https://plus.google.com/115291089467253743835" class="social-media-button ma-red" id="google-plus">
                    <span class="icon ma-google-plus"></span>
                </a>
            </div>
            <div class="cell-xs12 cell-md6 cell-td4 cell-tdl3">
                <a href="https://twitter.com/himatiftuhamka" class="social-media-button ma-sea" id="twitter">
                    <span class="icon ma-twitter"></span>
                </a>
            </div>
            <div class="cell-xs12 cell-md6 cell-td4 cell-tdl3">
                <a href="https://www.instagram.com/himati_uhamka/" class="social-media-button ma-brown" id="instagram">
                    <span class="icon ma-instagram"></span>
                </a>
            </div>
        </div>
    </div>
    <small>&copy; 2017 HIMA TI UHAMKA, All Right Reserved</small>
</div>

<style>
    .social-media-button {
        position: relative;
        display: block;
        margin: 50px auto;
        padding: 10%;
        width: 100px;
        height: 100px;
        border-radius: 100%;
        text-align: center;
        text-decoration: none;
        font-size: 30px;
        line-height: 75px;
        color: #fff;
        cursor: pointer;
        -webkit-transition: all .4s ease;
        -moz-transition: all .4s ease;
        transition: all .4s ease;
    }
    #footer {
        background: #eff4f5;
        color: #878fab;
        text-align: center;
        z-index: 9999999;
        position: relative;
        bottom: 0px;
        width: 100%;
    }
</style>

<!-- WhatsHelp.io widget -->
<script type="text/javascript">
    (function () {
        var options = {
            whatsapp: "08978329974", // WhatsApp number
            email: "muhammdafathan23@gmail.com", // Email
            call_to_action: "For More Information 😘", // Call to action
            button_color: "#129BF4", // Color of button
            position: "left", // Position may be 'right' or 'left'
            order: "whatsapp,email", // Order of buttons
        };
        var proto = document.location.protocol, host = "whatshelp.io", url = proto + "//static." + host;
        var s = document.createElement('script'); s.type = 'text/javascript'; s.async = true; s.src = url + '/widget-send-button/js/init.js';
        s.onload = function () { WhWidgetSendButton.init(host, proto, options); };
        var x = document.getElementsByTagName('script')[0]; x.parentNode.insertBefore(s, x);
    })();
</script>
