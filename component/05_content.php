<?php require("custom/05_content.html"); ?>
<div class="bungkus-content" id="content">
<!-- Seminar -->
<div class="parallax-container" id="seminar">
    <div class="parallax" data-aos="flip-right" data-aos-duration="2000" style="background-image: url('assets/landing/img/Seminar.jpg'); z-index: 999; "></div>
    <div class="parallax-caption">
        <h1 align="left" class="content-ifest" data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="800">
            Seminar
        </h1>
        <div class="caption-content">
            <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                <i> I-FEST </i> kembali mengadakan acara seminar dengan tema
                <b><i> Introduce Machine Learning NLP (Natural Language Processing) "Machine Learning For Machine On The Future" </i></b> Terdapat rangkaian acara lain seperti IT Expo berupa Bazar, Pameran Robotic dan masih banyak yang lainnya. Bergabung bersama kami sekarang juga!
                Jadi.. tunggu apalagi? Klik "More Info & Registration"
            </p>
        </div><br>
        <a href="javascript:void(0)" class="btn-slide btn-modal" data-target="m-seminar" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="1300"> 
            More Info & Registration 
        </a>
    </div>
</div>

<!-- Modal Seminar -->
<div class="modal-dialog" id="m-seminar">
    <div class="btn-close-modal" data-close="modal">
        <span class="icon ma-cross"></span>
    </div>
    <div class="title">
        <h1>Seminar</h1>
    </div>
    <div class="container clearfix">
        <div class="row">
            <div class="box cell-xs12">
                <div class="caption-content">
                    <p class="intro" data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                        Seminar <b><i>I-FEST 2019</i></b> membawa pembahasan mengenai Machine Learning NLP (Natural Language Processing). 
                        <br><br> 
                        NLP merupakan cabang aplikasi dari Artificial Intelligence (Kecerdasan Buatan) yang mempelajari struktur internal seseorang dan bagaimana struktur tersebut bisa didesain untuk tujuan yang bermanfaat bagi orang tersebut. Dalam NLP, setiap perilaku mempunyai struktur internal yang mendukungnya - <a href="http://socs.binus.ac.id/2013/06/22/natural-language-processing/"> derwin </a>
                        <br><br>
                        <div class="cell-td6 box-content">
                            <div class="img-place">
                                <img src="assets/images/yulianto.jpeg" class="img-ws">
                            </div>
                            <div class="caption">
                                <p> Muhammad Yulianto </p>
                                <span> Co Founder & Chief Technology Officer at IMPORTIR.ORG </span>
                            </div>
                        </div>
                        <div class="cell-td6 box-content">
                            <div class="img-place">
                                <img src="https://hacktiv8.com/img/programs/master-class/profile-luri.png__vzu2vhp2VRX%2Bewg7J0bPlaAd49d52b7c68be741a886b3afc9c04cb5" class="img-ws">
                            </div>
                            <div class="caption">
                                <p> Luri Darmawan </p>
                                <span> Technology Enthusiast & CTO at VIZITRIP.COM </span>
                            </div>
                        </div>
                        <div class="regist-kepo">
                            <p class="hidden-paragraph"> Pengen tau dengan pembahasannya? Tunggu apalagi? </p>
                            <br>
                            Pengen tau dengan pembahasannya? Tunggu apalagi? <br> Buruan daftar! Cek kolom <i>Registration</i> dibawah!
                        </div>
                    </p>
                    <br>
                    <div class="info">
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Tempat : Aula Ahmad Dahlan FKIP UHAMKA
                        </p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Tanggal Pelaksanaan : Sabtu, 13 Juli 2019
                        </p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Waktu : 08:00 - 16.00
                        </p>
                        <p><hr></p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            UMUM : Rp 65k
                        </p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            OTS : Rp 70k
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="cel-xs12">
                <form class="form-container" method="post">
                    <div class="form-content">
                        <div class="form-title">
                            <h1>Register</h1>
                        </div>
                        <input class="border-input" id="name" type="text" name="nama_sm" placeholder="Name" required>
                        <label for="name" class="label-border"></label>
                        <input class="border-input" id="email" type="email" name="email_sm" placeholder="Email" required>
                        <label for="email" class="label-border"></label>
                        <input class="border-input" id="hp" type="text" name="telp_sm" placeholder="No.Telepon" required>
                        <label for="hp" class="label-border"></label>
                        <input class="border-input" id="instansi" type="text" name="instansi_sm" placeholder="Instansi" required>
                        <label for="instansi" class="label-border"></label><br>
                        <input class="btn-register" name="regist-seminar" type="submit" value="Register">
                    </div>
                </form>
            </div>
        </div>
        <div class="row">
            <?php // require("component/12_alur.php"); ?>
        </div>
    </div>
</div>

<!-- Workshop -->
<div class="parallax-container" id="workshop">
    <div class="parallax" data-aos="flip-left" data-aos-duration="2000" style="background-image: url('assets/landing/img/Workshop.jpg'); z-index:999"></div>
    <div class="parallax-caption right">
        <h1 align="left" class="content-ifest" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="800">
            Workshop
        </h1>
        <div class="caption-content">
            <p data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                <i> I-FEST </i> kembali mengadakan acara workshop dengan Tema <b><i> "Improve The Experience With Data Science". </i></b>
                Bergabung bersama kami sekarang juga! <br> Jadi.. tunggu apalagi? Klik <i>"More Info & Registration"</i>
            </p>
        </div><br>
        <a href="javascript:void(0)" class="btn-slide btn-modal" data-target="m-workshop" data-aos="fade-up" data-aos-easing="ease-in-sine" data-aos-duration="1500"> 
            More Info & Registration 
        </a>
    </div>
</div>
<!-- Modal Workshop -->
<div class="modal-dialog" id="m-workshop">
    <div class="btn-close-modal" data-close="modal">
        <span class="icon ma-cross"></span>
    </div>
    <div class="title">
        <h1 data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="800">
            Workshop
        </h1>
    </div>
    <div class="container clearfix">
        <div class="row">
            <div class="box cell-xs12">
                <div class="caption-content">
                    <p class="intro" data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                        Workshop <b><i>I-FEST 2019</i></b> membawa pembahasan mengenai <i> "Machine Learning" </i>
                        <br><br>
                        <i> "Machine Learning </i> adalah cabang aplikasi dari <i> Artificial Intelligence </i> (Kecerdasan Buatan) yang fokus pada pengembangan sebuah sistem yang mampu belajar "sendiri" tanpa harus berulang kali di program oleh manusia" - <a href="http://teknosains.com/others/pengertian-konsep-dasar-machine-learning"> teknosains </a>
                        <br><br>
                        Pelatihan workshop <b><i>" Hands on Machine Learning Application with Python and BigData "</i></b>.
                        <div class="cell-td6 box-content">
                            <div class="img-place">
                                <img src="https://scontent-sjc3-1.cdninstagram.com/vp/d6987a69d9f6d2d613a4495bbaaf0f6c/5D925CB5/t51.2885-19/s320x320/53010822_1873372669434581_5818703538070487040_n.jpg?_nc_ht=scontent-sjc3-1.cdninstagram.com" class="img-ws">
                            </div>
                            <div class="caption">
                                <p><b> Afif A. Iskandar </b></p>
                                <span> AI Scientist, Bukalapak, Indonesia </span>
                            </div>
                        </div>
                        <div class="cell-td6 box-content">
                            <div class="img-place">
                                <img src="https://www.datascience.or.id/storage/people/cFesh4gLPfxSeqddzgiBXtEtUZTap2y18pT7oCIP.jpeg" class="img-ws">
                            </div>
                            <div class="caption">
                                <p><b> Alamsyah Hanza </b></p>
                                <span> Head Of Communication, Data Science Indonesia </span>
                            </div>
                        </div>
                        <div class="regist-kepo ws">
                            Pengen tau dengan pembahasan dan prakteknya langsung? Tunggu apalagi? 
                            <br>
                            Buruan daftar! Cek kolom <i>Registration</i> dibawah!
                        </div>
                    </p>
                    <br>
                    <div class="info">
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Tempat : Fakultas Teknik Uhamka
                        </p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Tanggal Pelaksanaan : Jumat, 05 Juli 2019
                        </p>
                        <p><hr></p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Waktu : 08:00 - 16.00
                        </p>
                        <p data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="1000">
                            Umum : Rp 50k
                        </p>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-xs-12">
                <img class="img-poster" src="assets/images/poster/Workshop.jpeg">
            </div>
        </div>
        <div class="row">
            <div class="cel-xs12">
                <form class="form-container" method="post">
                    <div class="form-content">
                        <div class="form-title">
                            <h1>Register</h1>
                        </div>
                        <input class="border-input" id="name" type="text" name="nama_ws" placeholder="Name" required>
                        <label for="name" class="label-border"></label>
                        <input class="border-input" id="email" type="email" name="email_ws" placeholder="Email" required>
                        <label for="email" class="label-border"></label>
                        <input class="border-input" id="hp" type="text" name="telp_ws" placeholder="No.Telepon" required>
                        <label for="hp" class="label-border"></label>
                        <input class="border-input" id="instansi" type="text" name="instansi_ws" placeholder="Instansi" required>
                        <label for="instansi" class="label-border"></label><br>
                        <input class="btn-register" name="#regist-workshop" type="submit" value="Register">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>

</div>
<?php require('process/pro_content.php'); ?>