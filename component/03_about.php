<?php require('custom/03_about.html'); ?>
<div class="about-ifest">
    <div class="container container-left" id="about">
        <div class="row">
            <div class="cell-td4">
                <div class="img-place about">
                    <a href="https://instagram.fcgk1-1.fna.fbcdn.net/vp/66ee7bdda716650026733fff1b6fc8ed/5DA13017/t51.2885-15/sh0.08/e35/s640x640/64726279_322898011992367_2235888766386940626_n.jpg?_nc_ht=instagram.fcgk1-1.fna.fbcdn.net&_nc_cat=107%20640w,https://instagram.fcgk1-1.fna.fbcdn.net/vp/f121dd362681aabf70cb0c443cc69f92/5D93C217/t51.2885-15/sh0.08/e35/s750x750/64726279_322898011992367_2235888766386940626_n.jpg?_nc_ht=instagram.fcgk1-1.fna.fbcdn.net&_nc_cat=107%20750w,https://instagram.fcgk1-1.fna.fbcdn.net/vp/b565a0234bccf0c7f868a5ae6ce7c3a3/5DBF36A0/t51.2885-15/e35/s1080x1080/64726279_322898011992367_2235888766386940626_n.jpg?_nc_ht=instagram.fcgk1-1.fna.fbcdn.net&_nc_cat=107%201080w" target="_blank">
                        <img data-aos="zoom-in-up" data-aos-duration="2000" src="assets/landing/img/Poster-IFestV2.jpeg" class="img-about animated fadeIn">
                    </a>
                </div>
                <!-- The Modal -->
                <div id="myModal" class="modal">
                    <span class="close">&times;</span>
                    <img class="modal-content" id="img01">
                    <div id="caption"></div>
                </div>
            </div>
            <div class="cell-td8">
                <div class="caption-content">
                    <h1 class="about-title" data-aos="fade-up" data-aos-duration="1000"> ABOUT I-FEST </h1>
                    <h1 class="sub-about-title" data-aos="fade-up" data-aos-duration="1000"> THE BIGGEST IT FESTIVAL AT UHAMKA </h1>
                    <p class="content animated" data-aos="fade-up" data-aos-duration="500">
                        <b style="font-weight: 900; font-family: monospace;"> I-FEST </b> 
                        merupakan acara tahunan yang diadakan HIMA TI UHAMKA bertujuan sebagai wadah untuk mengembangkan wawasan dan kemampuan IT untuk bisa diimplementasikan ke kehidupan sehari-hari. Selain itu juga demi meningkatkan kualitas SDM untuk bersaing khususnya dalam bidang IT. 
                        <br>
                        <br> Tahun ini merupakan tahun ke empat kami mengadakan acara ini, dari 
                        <b style="font-weight: 900; font-family: monospace;"> I-FEST </b> sebelumnya yang sudah sukses dilaksanakan. 
                    </p>
                    <!-- <a href="about/" class="btn-about-more" data-aos="fade-up" data-aos-duration="1500"> Read More </a> -->
                    <a href="#content" class="btn-about-more" data-aos="fade-up" data-aos-duration="1500"> Join With Us </a>
                </div>
            </div>
        </div>
    </div>
</div>
<script>
// Get the modal
var modal = document.getElementById("myModal");

// Get the image and insert it inside the modal - use its "alt" text as a caption
var img = document.getElementById("myImg");
var modalImg = document.getElementById("img01");
var captionText = document.getElementById("caption");
img.onclick = function(){
  modal.style.display = "block";
  modalImg.src = this.src;
  captionText.innerHTML = this.alt;
}

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on <span> (x), close the modal
span.onclick = function() { 
  modal.style.display = "none";
}
</script>