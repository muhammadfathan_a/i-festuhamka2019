<!-- IT Care -->
<div class="content" id="it-care">
    <div class="title">
        <h1>IT Care</h1>
        <hr class="hr-gradient">
    </div>
    <svg version="1.1" class="logo-itcare" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" x="0px" y="0px" viewBox="0 0 1280 800" style="enable-background:new 0 0 1280 800;" xml:space="preserve">
    <style type="text/css">
        .st0{fill:url(#SVGID_1);}
        .st1{fill:#FFFFFF;}
        .st2{fill:url(#SVGID_2);}
        .st3{fill:url(#SVGID_3);}
        .st4{fill:#B3B3B3;}
    </style>
    <linearGradient id="SVGID_1" gradientUnits="userSpaceOnUse" x1="40.6368" y1="284.6368" x2="222.3632" y2="466.3632">
        <stop  offset="0" style="stop-color:#02CC83"/>
        <stop  offset="1" style="stop-color:#00CCFF"/>
    </linearGradient>
    <linearGradient id="SVGID_2" gradientUnits="userSpaceOnUse" x1="551.6368" y1="284.6368" x2="733.3632" y2="466.3632">
        <stop  offset="0" style="stop-color:#02CC83"/>
        <stop  offset="1" style="stop-color:#00CCFF"/>
    </linearGradient>
    <linearGradient id="SVGID_3" gradientUnits="userSpaceOnUse" x1="1058.6367" y1="285.6368" x2="1240.3633" y2="467.3632">
        <stop  offset="0" style="stop-color:#02CC83"/>
        <stop  offset="1" style="stop-color:#00CCFF"/>
    </linearGradient>
    <a href="javascript:void(0)" class="btn-modal" data-target="games">	<!--  Games  -->
    <circle class="st0" cx="131.5" cy="375.5" r="128.5"/>
    <g>
        <path class="st1" d="M114.7,361.1c1.1,0,2.1-0.3,3-0.8c0.7,1.6,1.1,3.4,1.1,5.4c0,1.4-0.2,2.7-0.6,3.9c-1-0.7-2.2-1.2-3.6-1.2
            c-3.3,0-6.1,2.7-6.1,6.1c0,1.4,0.5,2.7,1.3,3.7c-1.4,0.5-2.8,0.7-4.3,0.7c-1.5,0-3-0.3-4.3-0.7c0.8-1,1.3-2.3,1.3-3.7
            c0-3.3-2.7-6.1-6.1-6.1c-1.3,0-2.6,0.4-3.6,1.2c-0.4-1.2-0.6-2.6-0.6-3.9c0-1.9,0.4-3.7,1.1-5.4c0.9,0.5,1.9,0.8,3,0.8
            c3.3,0,6.1-2.7,6.1-6.1c0-0.8-0.1-1.5-0.4-2.2c1.1-0.3,2.2-0.5,3.4-0.5c1.2,0,2.3,0.2,3.4,0.5c-0.3,0.7-0.4,1.4-0.4,2.2
            C108.7,358.4,111.4,361.1,114.7,361.1z M87,348.2l2.1-2.6h-4.3L87,348.2z M93.7,336.9v4.3l2.6-2.1L93.7,336.9z M142.3,368.6v-3.8
            l-2.3,1.9L142.3,368.6z M84.9,332.5h4.3l-2.1-2.6L84.9,332.5z M146.1,360.9h3.8l-1.9-2.3L146.1,360.9z M153.9,368.6l2.3-1.9
            l-2.3-1.9V368.6z M80.6,341.2v-4.3L78,339L80.6,341.2z M189.7,438.7c-23.1,0-20.3-49.8-54.9-49.8c-42.7,0-32.5,50.3-63.7,50.3
            c-37.1,0-25.9-125.5,23.7-125.5c0,0,14,6.9,32.9,9.3l11.2,0c18.2-2.4,29.6-9,29.6-9C209.5,313.9,226.7,438.7,189.7,438.7z
                M140.5,336.9c0,2.4,1.9,4.3,4.3,4.3s4.3-1.9,4.3-4.3c0-2.4-1.9-4.3-4.3-4.3S140.5,334.6,140.5,336.9z M127.9,329.5
            c0.8,0.5,1.6,1.1,2.4,1.8c0.8-0.7,1.6-1.3,2.4-1.8c-0.7-0.3-1.5-0.5-2.4-0.5C129.4,329.1,128.6,329.2,127.9,329.5z M87,349.9
            c6,0,10.9-4.9,10.9-10.9c0-6-4.9-10.9-10.9-10.9c-6,0-10.9,4.9-10.9,10.9C76.1,345.1,81,349.9,87,349.9z M120.2,365.7
            c0-8-6.5-14.5-14.5-14.5c-8,0-14.5,6.5-14.5,14.5c0,8,6.5,14.5,14.5,14.5C113.7,380.2,120.2,373.7,120.2,365.7z M121.2,336.9
            c0-2.4-1.9-4.3-4.3-4.3c-2.4,0-4.3,1.9-4.3,4.3c0,2.4,1.9,4.3,4.3,4.3C119.3,341.2,121.2,339.3,121.2,336.9z M124.7,338.5
            c0.2-0.4,0.4-0.8,0.6-1.3c0.6-1.1,1.4-2.3,2.4-3.4c-0.8-1-1.5-1.9-2.1-2.8c-1.1,1.2-1.8,2.7-1.8,4.5
            C123.9,336.6,124.2,337.6,124.7,338.5z M135.2,339.4c-0.4-0.2-0.9-0.4-1.5-0.7c-1.1-0.6-2.3-1.4-3.4-2.4c-1.2,1-2.4,1.9-3.4,2.4
            c-0.6,0.3-1.1,0.6-1.5,0.7c1.2,1.4,2.9,2.4,4.9,2.4C132.3,341.8,134,340.9,135.2,339.4z M136.6,335.4c0-1.7-0.7-3.3-1.8-4.5
            c-0.6,0.9-1.3,1.9-2.1,2.8c1,1.2,1.9,2.4,2.4,3.4c0.3,0.5,0.5,0.9,0.7,1.3C136.3,337.6,136.6,336.6,136.6,335.4z M157.7,366.7
            c0-5.4-4.3-9.7-9.7-9.7c-5.4,0-9.7,4.3-9.7,9.7c0,5.4,4.3,9.7,9.7,9.7C153.3,376.4,157.7,372.1,157.7,366.7z M169.1,348
            c0-2.6-2.1-4.6-4.6-4.6c-2.6,0-4.6,2.1-4.6,4.6c0,2.6,2.1,4.6,4.6,4.6C167,352.6,169.1,350.6,169.1,348z M169.1,336.5
            c0,2.6,2.1,4.6,4.6,4.6c2.6,0,4.6-2.1,4.6-4.6c0-2.6-2.1-4.6-4.6-4.6C171.2,331.8,169.1,333.9,169.1,336.5z M179.4,357.3
            c0-2.6-2.1-4.6-4.6-4.6s-4.6,2.1-4.6,4.6c0,2.6,2.1,4.6,4.6,4.6S179.4,359.8,179.4,357.3z M188.7,345.7c0-2.6-2.1-4.6-4.6-4.6
            c-2.6,0-4.6,2.1-4.6,4.6c0,2.6,2.1,4.6,4.6,4.6C186.6,350.4,188.7,348.3,188.7,345.7z M148,374.8l1.9-2.3h-3.8L148,374.8z"/>
    </g>
    </a>
    <a href="javascript:void(0)" class="btn-modal" data-target="driver">	<!--  Driver  -->
    <circle class="st2" cx="642.5" cy="375.5" r="128.5"/>
    <path class="st1" d="M616.8,297.7c13.4,11.4,24.6,23.3,33.1,36l-46.2,19.3c-2.2,0.9-4.5,1.9-6.5,3.1l-14.5,7.8
        c-22.1,12.8-27.7,23.1-3.6,29.5c-8.1-0.2-14.3-1.6-19.3-5.2c-3.1-2.2-4.8-5.9-5.2-9.7c-1.2-16.7,3.8-33.6,13.8-50.7
        c8.8-12.1,21.7-22.6,38.8-31.2C610.2,295,614,295.5,616.8,297.7z"/>
    <path class="st1" d="M662.8,355.8l-75.5,33.6c-30.3,14.1-29.3,28.1-4.8,51.7c16.7,13.6,36.2,22.2,60.8,21.5
        c19.8-0.2,32.7-7.2,37.1-22.9c3.4-19,0.2-38.8-6.9-59.1C670.2,369.9,666.6,361.7,662.8,355.8z"/>
    <path class="st1" d="M683.5,358.4l6,19c5.5,22.7,8.6,43.9,4,58.6c-4,8.3-9,12.9-17.2,15.5c9.7,2.2,16.9-2.2,26.4-11.7
        c17.8-20.9,26.9-35.3,26.4-73.1c-0.3-7.4-2.2-15.7-4.8-22.6c-4.7-4.5-10.5-5.2-20.7-4.3c-10.5,2.8-18.6,6.2-22.4,11.7L683.5,358.4z"
        />
    <path class="st1" d="M670.7,330c-9-12.6-19.5-23.4-31.2-32.6c-7.2-4.8-13.1-3.1-17.9,3.6c0.7-8.4,6-12.2,14.3-12.9
        c11.5-1,24.3,2.2,37.7,7.1s25.2,12.8,35.8,22.9c2.2,2.1,4.1,4.3,5.7,7.1c3.3,5.9,2.9,10.5-1,13.6c2.1-5.2,1.2-9.1-2.8-11.7
        c-1.7-1-3.6-1.7-5.5-1.9C693,324.1,681.4,325.6,670.7,330z"/>
    </a>
    <a href="javascript:void(0)" class="btn-modal" data-target="software">	<!--  Software  -->
    <circle class="st3" cx="1149.5" cy="376.5" r="128.5"/>
    <polygon class="st1" points="1080.5,317 1080.5,441.3 1104.3,448.6 1104.3,304.8 "/>
    <path class="st1" d="M1110.2,302v147.3l31.5-7c-35.4-50.8,21.7-97,63.7-67.6l0.4-33.6c-2.1-13.3-9.7-20.7-20-24.9L1110.2,302z"/>
    <path class="st1" d="M1176.7,370.9c-22.3,0-40.3,18.1-40.3,40.3s18.1,40.3,40.3,40.3s40.3-18.1,40.3-40.3S1198.9,370.9,1176.7,370.9
        z M1179.3,419.9c-4.9,0-9-4.1-9-9c0-4.9,4.1-9,9-9c4.9,0,9,4.1,9,9C1188.3,415.8,1184.2,419.9,1179.3,419.9z"/>
    </a>
    <g>
        <rect x="335.1" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="311.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="288.3" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="405.4" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="382" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="358.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="475.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="452.3" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="429" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="842.1" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="818.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="795.3" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="912.4" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="889" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="865.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="982.7" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="959.3" y="376.3" class="st4" width="13.1" height="7.5"/>
        <rect x="936" y="376.3" class="st4" width="13.1" height="7.5"/>
    </g>
    </svg>
    
<!-- Modal Games -->
    <div class="modal-dialog" id="games">
        <div class="btn-close-modal" data-close="modal">
            <span class="icon ma-cross"></span>
        </div>
        <div class="title">
            <h1>Games</h1>
            <hr class="hr-gradient">
        </div>
        <div class="flat-card-container">
            <div class="flat-card-header ma-red">
                <div class="flat-menu">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
                <div class="flat-viewer">
                    <span class="icon ma-tiles-medium"></span>
                </div>
                <div class="flat-search">
                    <input type="text" name="" id="search" placeholder="Search Games">
                    <label for="search" class="tooltip-icon icon ma-loop-search"></label>
                </div>
            </div>
            <div class="flat-card-body">
                <div class="clearfix">
                    <div class="row">
                        <div class="cell-xs12">
                            <div class="img-place">
                                <img src="assets/landing/img/Games/Bg-Battlefield-1.jpg">
                                <span class="img-overlay"></span>
                            </div>
                        </div>
                        <div class="cell-xs12">
                            <div class="flat-card-caption">
                                <div class="flat-card-place">
                                    <div class="flat-card">
                                        <div class="img-place">
                                            <img src="assets/landing/img/Games/M_Battlefield-1.jpg">
                                        </div>
                                    </div>
                                    <div class="caption">
                                        <div class="caption-card ma-red">Free</div>
                                        <div class="bookmark-card ma-hover-red"><span class="icon ma-bookmark"></span></div>
                                    </div>
                                </div>
                                <div class="flat-caption-info">
                                    <span class="rating ma-red"><span class="icon ma-star"></span> 4.3</span>
                                    <p class="c-black flat-title">Battlefield 1</p>
                                    <div class="label-container">
                                        <span class="label">RPG</span>
                                        <span class="label">Action</span>
                                        <span class="label">Shooter</span>
                                    </div>
                                    <p>Battlefield 1 takes you back to The Great War (World War 1),
                                        where new technology and worldwide conflict changed the face of warfare forever.
                                        Take part in every battle, control every massive vehicle, and execute every maneuver that turns an entire fight around.
                                        The whole world is at war – see what’s beyond the trenches.</p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="flat-card-content" id="content-games">
            <!-- New Added Games -->
                <div class="flat-box-container clearfix">
                    <div class="flat-title-card" id="tag-new">
                        <span>New Added</span>
                    </div>
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Battlefield-1.jpg">
                                </div>
                                <div class="caption">
                                    <span>Battlefield-1</span>
                                    <small>Electronic Art</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Battlefield-4.jpg">
                                </div>
                                <div class="caption">
                                    <span>Battlefield 3</span>
                                    <small>Electronic Art</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_GTA-5.jpg">
                                </div>
                                <div class="caption">
                                    <span>Grand Theft Auto V</span>
                                    <small>Rockstar Games</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Far_Cry-4.jpg">
                                </div>
                                <div class="caption">
                                    <span>Far Cry 4</span>
                                    <small>Ubisoft</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Watch_Dogs.jpg">
                                </div>
                                <div class="caption">
                                    <span>Watch Dogs</span>
                                    <small>Ubisoft</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_COD_AW.jpg">
                                </div>
                                <div class="caption">
                                    <span>Call of Duty Advanced Warfare</span>
                                    <small>Activision</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_PES-2017.jpg">
                                </div>
                                <div class="caption">
                                    <span>Pro Evolution Soccer 2017</span>
                                    <small>Konami</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="btn-see-more"><small>See More <span class="icon ma-circle-play"></small></span></p>
                </div>
                <div class="full-card-container ma-blue">
                    <div class="img-place">
                        <img src="assets/landing/img/Games/Watch-Dogs.jpg">
                        <span class="img-dark-overlay"></span>
                        <span class="img-caption">
                            <p class="caption-title">Watch Dogs</p>
                            <p>Watch Dogs is an open world action-adventure third-person <br> shooter video game developed and published by Ubisoft</p><br>
                            <span class="btn-slide">Visit Site</span>
                        </span>
                    </div>
                </div>
            <!-- End of New Added Games -->
            
            <!-- Popular-Games -->
                <div class="flat-box-container clearfix">
                    <div class="flat-title-card" id="tag-top">
                        <span>Popular</span>
                    </div>
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_NFS_MW-2012.jpg">
                                </div>
                                <div class="caption">
                                    <span>Need For Speed Most Wanted 2012</span>
                                    <small>Electronic Art</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Battlefield-1.jpg">
                                </div>
                                <div class="caption">
                                    <span>Battlefield 1</span>
                                    <small>Electronic Art</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_COD_MW-4.jpg">
                                </div>
                                <div class="caption">
                                    <span>Call of Duty Modern Warfare 4</span>
                                    <small>Activision</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Far_Cry-3.jpg">
                                </div>
                                <div class="caption">
                                    <span>Far Cry 3</span>
                                    <small>Ubisoft</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_PES-2016.jpg">
                                </div>
                                <div class="caption">
                                    <span>Pro Evolution Soccer 2016</span>
                                    <small>Konami</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_Grid-2.jpg">
                                </div>
                                <div class="caption">
                                    <span>Grid 2</span>
                                    <small>Codemaster</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_ACS.jpg">
                                </div>
                                <div class="caption">
                                    <span>Assasin's Creed Syndicate</span>
                                    <small>Ubisoft</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Games/M_GTA-4.jpg">
                                </div>
                                <div class="caption">
                                    <span>Grand Theft Auto IV</span>
                                    <small>Rockstar Games</small>
                                </div>
                            </div>
                        </div>
                    </div>
                    <p class="btn-see-more"><small>See More <span class="icon ma-circle-play"></small></span></p>
                </div>
                <div class="full-card-container ma-red">
                    <div class="img-place">
                        <img src="assets/landing/img/Games/NFS_Most_Wanted-2012.jpg">
                        <span class="img-dark-overlay"></span>
                        <span class="img-caption">
                            <p class="caption-title">NFS Most Wanted 2012</p>
                            <p>Need for Speed: Most Wanted is an open world racing game <br> developed by Criterion Games and published by Electronic Arts</p><br>
                            <span class="btn-slide">Visit Site</span>
                        </span>
                    </div>
                </div> 
            <!-- End of Popular Games -->
            </div>
        </div>
    </div>
<!-- End of Modal Games -->

<!-- Modal Driver -->
    <div class="modal-dialog" id="driver">
        <div class="btn-close-modal" data-close="modal">
            <span class="icon ma-cross"></span>
        </div>
        <div class="title">
            <h1>Driver</h1>
            <hr class="hr-gradient">
        </div>
        <div class="flat-card-container">
            <div class="flat-card-header ma-orange">
                <div class="flat-menu">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
                <div class="flat-viewer">
                    <span class="icon ma-tiles-medium"></span>
                </div>
                <div class="flat-search">
                    <input type="text" name="" id="search" placeholder="Search Driver">
                    <label for="search" class="tooltip-icon icon ma-loop-search"></label>
                </div>
            </div>
            <div class="flat-card-content" id="content-driver">
                <div class="flat-title">
                    <p>Driver for all Brand</p>
                </div>
                <div class="flat-box-container clearfix">
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Asus-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Acer-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Dell-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Lenovo-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/MSI-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Sony-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Hp-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Samsung-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Fujitsu-brand.png">
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Brand/Toshiba-brand.png">
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="flat-title">
                    <p>Support for any Driver</p>
                </div>
                <div class="flat-box-container clearfix">
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Bluetooth.png">
                                </div>
                                <div class="caption">
                                    <span>Bluetooth</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Mouse.png">
                                </div>
                                <div class="caption">
                                    <span>Mouse</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Keyboard.png">
                                </div>
                                <div class="caption">
                                    <span>Keyboard</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/CD.png">
                                </div>
                                <div class="caption">
                                    <span>Blu-ray/DVD/CD</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Disk.png">
                                </div>
                                <div class="caption">
                                    <span>Disk Drive</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Processor.png">
                                </div>
                                <div class="caption">
                                    <span>Processor</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Joystick.png">
                                </div>
                                <div class="caption">
                                    <span>Joystick</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/USB.png">
                                </div>
                                <div class="caption">
                                    <span>Universal Serial Bus</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Webcam.png">
                                </div>
                                <div class="caption">
                                    <span>Webcam</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Network.png">
                                </div>
                                <div class="caption">
                                    <span>Network</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Video Graphics.png">
                                </div>
                                <div class="caption">
                                    <span>Video Graphics</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Speaker.png">
                                </div>
                                <div class="caption">
                                    <span>Sound</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Modem.png">
                                </div>
                                <div class="caption">
                                    <span>Modem</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Wifi.png">
                                </div>
                                <div class="caption">
                                    <span>Wifi</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Scanner.png">
                                </div>
                                <div class="caption">
                                    <span>Scanner</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Printer.png">
                                </div>
                                <div class="caption">
                                    <span>Printer</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Smartphone.png">
                                </div>
                                <div class="caption">
                                    <span>Smartphone</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Tablet.png">
                                </div>
                                <div class="caption">
                                    <span>Tablet</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Cardreader.png">
                                </div>
                                <div class="caption">
                                    <span>Cardreader</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/TV Tunner.png">
                                </div>
                                <div class="caption">
                                    <span>TV Tunner</span>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Driver/Monitor.png">
                                </div>
                                <div class="caption">
                                    <span>Monitor</span>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- End of Modal Driver -->

<!-- Modal Software -->
    <div class="modal-dialog" id="software">
        <div class="btn-close-modal" data-close="modal">
            <span class="icon ma-cross"></span>
        </div>
        <div class="title">
            <h1>Software</h1>
            <hr class="hr-gradient">
        </div>
        <div class="flat-card-container">
            <div class="flat-card-header ma-blue">
                <div class="flat-menu">
                    <span class="bar"></span>
                    <span class="bar"></span>
                    <span class="bar"></span>
                </div>
                <div class="flat-viewer">
                    <span class="icon ma-tiles-medium"></span>
                </div>
                <div class="flat-search">
                    <input type="text" name="" id="search" placeholder="Search Software">
                    <label for="search" class="tooltip-icon icon ma-loop-search"></label>
                </div>
            </div>
            <div class="flat-card-content" id="content-software">
            
            <!-- Popular Software -->
                <div class="flat-box-container clearfix">
                    <div class="flat-title-card" id="tag-top">
                        <span>Popular</span>
                    </div>
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Chrome.jpg">
                                </div>
                                <div class="caption">
                                    <span>Google Chrome</span>
                                    <small>Google Inc</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/AVG.jpg">
                                </div>
                                <div class="caption">
                                    <span>AVG</span>
                                    <small>AVG Technologies</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Ccleaner.jpg">
                                </div>
                                <div class="caption">
                                    <span>Ccleaner</span>
                                    <small>Priform</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Winrar.jpg">
                                </div>
                                <div class="caption">
                                    <span>WinRAR</span>
                                    <small>RARLab</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Firefox.jpg">
                                </div>
                                <div class="caption">
                                    <span>Firefox</span>
                                    <small>Mozilla Organization</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Kaspersky.jpg">
                                </div>
                                <div class="caption">
                                    <span>Kaspersky</span>
                                    <small>Kaspersky Lab</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Avira.jpg">
                                </div>
                                <div class="caption">
                                    <span>Avira</span>
                                    <small>Avira GmbH</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Opera.jpg">
                                </div>
                                <div class="caption">
                                    <span>Opera</span>
                                    <small>Opera Software</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Microsoft Office.jpg">
                                </div>
                                <div class="caption">
                                    <span>Microsoft Office</span>
                                    <small>Microsoft Corporation</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/VLC.jpg">
                                </div>
                                <div class="caption">
                                    <span>VLC Media Player</span>
                                    <small>VideoLAN</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/IDM.jpg">
                                </div>
                                <div class="caption">
                                    <span>Internet Download Manager</span>
                                    <small>Tonec Inc</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div> 
            <!-- End of Popular Software -->
            
            <!-- Recomended Software -->
                <div class="flat-box-container clearfix">
                    <div class="flat-title-card" id="tag-recomended">
                        <span>Recomended</span>
                    </div>
                    <div class="row">
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Microsoft Office.jpg">
                                </div>
                                <div class="caption">
                                    <span>Microsoft Office</span>
                                    <small>Microsoft Corporation</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Chrome.jpg">
                                </div>
                                <div class="caption">
                                    <span>Google Chrome</span>
                                    <small>Google Inc</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/AVG.jpg">
                                </div>
                                <div class="caption">
                                    <span>AVG</span>
                                    <small>AVG Technologies</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Firefox.jpg">
                                </div>
                                <div class="caption">
                                    <span>Firefox</span>
                                    <small>Mozilla Organization</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Bitdefender.jpg">
                                </div>
                                <div class="caption">
                                    <span>Bitdefender</span>
                                    <small>Bitdefender</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Opera.jpg">
                                </div>
                                <div class="caption">
                                    <span>Opera</span>
                                    <small>Opera Software</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/IDM.jpg">
                                </div>
                                <div class="caption">
                                    <span>Internet Download Manager</span>
                                    <small>Tonec Inc</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/VLC.jpg">
                                </div>
                                <div class="caption">
                                    <span>VLC Media Player</span>
                                    <small>VideoLAN</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Norton.jpg">
                                </div>
                                <div class="caption">
                                    <span>Norton Security 2015</span>
                                    <small>Symantec</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Adobe CS6.jpg">
                                </div>
                                <div class="caption">
                                    <span>Adobe CS6 Collection</span>
                                    <small>Adobe</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Winrar.jpg">
                                </div>
                                <div class="caption">
                                    <span>Winrar</span>
                                    <small>RARLab</small>
                                </div>
                            </div>
                        </div>
                        <div class="cell-md6 cell-td4 cell-tdl3 cell-dd2">
                            <div class="flat-box">
                                <div class="img-place">
                                    <img src="assets/landing/img/Software/Cyberlink.jpg">
                                </div>
                                <div class="caption">
                                    <span>CyberLink YouCam</span>
                                    <small>CyberLink Corp</small>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- End of Modal Software -->
</div>