<?php require('custom/02_header.html'); ?>

<div class="bg-animation" data-aos="zoom-in-left" data-aos-duration="2000">
	<div id="img-place" data-aos="zoom-in-left" data-aos-duration="2000"></div>
	<div class="caption">
		<div class="caption-banner">
			<h1 class="c1" data-aos="fade-right" data-aos-easing="ease-in-sine" data-aos-duration="2500">
				LIGHT UP <br> WITH TECHNOLOGY <br> IN OVER SMART AND <br> INTEGRATED ERA 
			</h1>
			<br>
			<p class="sub-caption" data-aos="fade-left" data-aos-easing="ease-in-sine" data-aos-duration="2300"> 
				Time to create impact and amplify awareness.
			</p>
			<!-- <img src="assets/images/AI.png" class="AI" alt="ai"> -->
		</div>
	</div>
</div>
<div class="package-btn">
	<div class="header-btn">
		<b><a href="#about" class="btn-letsgo" data-aos="zoom-in-top" data-aos-easing="ease-in-sine" data-aos-duration="2500" data-aos-offset="-500"> 
			LET'S GO! 
		</a></b>
	</div>
</div>