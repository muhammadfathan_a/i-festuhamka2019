<?php require('custom/04_competition.html'); ?>

<div class="content" id="competition">
<div class="content-card">
    <div class="title" data-aos="fade-right" data-aos-duration="500">
        <h1 class="content-title">COMPETITION</h1>
    </div>
    <div class="caption-content-competition">
        <p data-aos="fade-up-right" data-aos-duration="1000">
            <b class="ifest-competiion"> I-FEST </b> 
            kembali hadir dengan berbagai jenis kompetisi yang ditunggu oleh setiap penggiat IT di Indonesia. 
            <b class="ifest-competiion"> I-FEST </b> mewadahi masyarakat Indonesia lewat <b class="ifest-competiion"><i> I-FEST Competition </i></b>, untuk bersaing dalam skill di bidang IT. </p>
        <center>
            <a href="javascript:void(0)"  data-aos="fade-up-right" data-aos-duration="1500" class="btn-slide btn-modal" data-target="m-competition" id="btn-competition" data-aos-offset="-300"> 
                Download Guide Book 
            </a>
        </center>
    </div>
    <div class="kuy-join" data-aos="zoom-in-up" data-aos-offset="-300" data-aos-duration="1000">
        <h1 class="kuy" data-aos="zoom-in-up" data-aos-offset="-300" data-aos-duration="1000">  TUNGGU APALAGI? </h1>
        <h6 class="yxgkuy" data-aos="zoom-in-left" 
            data-aos-offset="-300" data-aos-duration="1500" 
            style="font-size: 21px; font-weight: 600;"> 
            BURUAN DAFTAR!!! 
        </h6>
    </div>
</div>
    <div class="container container-left">
        <br>
        <div class="package-row-img" data-aos="zoom-in-left" data-aos-duration="800">
        <div class="row">
            <div class="cell-md6 cell-td4 cell-tdl3">
                <div class="card-container">
                    <div class="card-content">
                        <div class="img-place">
                            <img class="img-comp" data-aos="zoom-in-up" data-aos-duration="1000" src="assets/images/ProgressiveCoding.png">
                        </div>
                        <div class="caption">
                            <p data-aos="zoom-in" data-aos-duration="1200"> Progressive <br> Coding </p>
                            <a href="javascript:void(0)" id="test" class="btn-slide btn-modal" data-target="m-mobile-apps" data-aos="zoom-in-right" data-aos-duration="1000" >
                                Register
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-dialog half anim scale-out" id="m-mobile-apps">
                <div class="modal-overlay"></div>
                <div class="modal-content pa">
                    <div class="form-card">
                        <div class="form-header">
                            <span class="btn-back icon ma-chevron-left" data-close="modal"></span>
                            <span class="title-form"> Progressive Coding! </span>
                        </div>
                        <div class="form-place">
                            <form class="form-content" method="post">
                                <input class="border-input" id="name" type="text" name="nama_team_pc" placeholder="Nama Tim" required>
                                <label for="name" class="label-border"></label>
                                <input class="border-input" id="email" type="email" name="email_pc" placeholder="Email" required>
                                <label for="email" class="label-border"></label>
                                <input class="border-input" id="password" type="password" name="password_pc" placeholder="Password" required>
                                <label for="email" class="label-border"></label>
                                <input type="hidden" name="level" value="mac">
                                <input class="btn-register" name="regist_pc" type="submit" value="Register">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
            <div class="cell-md6 cell-td4 cell-tdl3">
                <div class="card-container">
                    <div class="card-content">
                        <div class="img-place">
                            <img class="img-comp" data-aos="zoom-in-up" data-aos-duration="1000" src="assets/images/MobileApp.png">
                        </div>
                        <div class="caption">
                            <p data-aos="zoom-in" data-aos-duration="1200"> Mobile App <br> UI / UX </p>
                            <a href="javascript:void(0)" class="btn-slide btn-modal" data-target="m-web-design" data-form-target="f-web-design" data-aos="zoom-in-left" data-aos-duration="1000" >
                                Register
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-dialog half anim scale-out" id="m-web-design">
                <div class="modal-overlay"></div>
                <div class="modal-content ma">
                    <div class="form-card">
                        <div class="form-header">
                            <span class="btn-back icon ma-chevron-left" data-close="modal"></span>
                            <span class="title-form"> Mobile App UI/UX </span>
                        </div>
                        <div class="form-place">
                            <form class="form-content" method="post">
                                <input class="border-input" id="name" type="text" name="nama_team_ma" placeholder="Nama Tim" required>
                                <label for="name" class="label-border"></label>
                                <input class="border-input" id="email" type="email" name="email_ma" placeholder="Email" required>
                                <label for="email" class="label-border"></label>
                                <input class="border-input" id="password" type="password" name="password_ma" placeholder="Password" required>
                                <label for="email" class="label-border"></label>
                                <input class="btn-register" name="regist_ma" type="submit" value="Register">
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </div>
    <br>
    <br>
</div>

<!-- Modal Competition -->
<div class="modal-dialog" id="m-competition">
    <div class="btn-close-modal" data-close="modal">
        <span class="icon ma-cross"></span>
    </div>
    <div class="title">
        <h1 class="tit-comp">Competition</h1>
    </div>
    <div class="tabs-container">
        <div id="tabs-button">
            <div href="javascript:void(0)" class="btn-tabs active ma-hover-lime" data-tab="web-design">
                <p> Pro. Coding </p>
            </div>
            <div href="javascript:void(0)" class="btn-tabs ma-hover-lime" data-tab="poster-design">
                <p> Mobile Apps </p>
            </div>
        </div>
        <div class="tabs-content">
            <div class="tabs active clearfix" id="web-design">
                <div class="row">
                    <div class="cell-td4 pc">
                        <div class="img-place pc">
                            <img src="assets/images/ProgressiveCoding.png" class="img-pc">
                        </div>
                    </div>
                    <div class="cell-td8">
                        <div class="tabs-caption">
                            <div class="tabs-title">
                                <h2 class="title-pc"> Progressive Coding! </h2>
                                <hr>
                            </div>
                            <div class="tabs-caption-pc">
                                <p> </p>
                                <p> Tempat : Fakultas Teknik UHAMKA </p>
                                <p> Tanggal Pelaksanaan : Sabtu, 06 Juli 2019 </p>
                                <p> Waktu : 08.00 - 16.00 </p>
                                <p> Umum : Rp 120.000 </p>
                                <a href="https://drive.google.com/open?id=19mPXCzaaouLv3iW1MVW-Vehyi7d_G2Hy" target="_blank" class="btn-full-page"> Guide Book </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="tabs clearfix" id="poster-design">
                <div class="row">
                    <div class="cell-td4 pc">
                        <div class="img-place ma">
                            <img src="assets/images/MobileApp.png" class="ma">
                        </div>
                    </div>
                    <div class="cell-td8">
                        <div class="tabs-caption">
                            <div class="tabs-title">
                                <h2 class="title-ma"> Mobile App's UI/UX </h2>
                                <hr>
                            </div>
                            <div class="tabs-caption-pc">
                                <p> </p>
                                <p> Tempat : Fakultas Teknik UHAMKA </p>
                                <p> Tanggal Pelaksanaan : Sabtu, 06 Juli 2019 </p>
                                <p> Waktu : 08.00 - 16.00 </p>
                                <p> Umum : Rp 100.000 </p>
                                <a href="https://drive.google.com/open?id=19mPXCzaaouLv3iW1MVW-Vehyi7d_G2Hy" target="_blank" class="btn-full-page"> Guide Book </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php require('process/pro_competition.php'); ?>