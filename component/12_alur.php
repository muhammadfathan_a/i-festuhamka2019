<?php require('custom/12_alur.html'); ?>
<section class="colorlib-experience" data-section="experience">
    <div class="colorlib-narrow-content">
        <div class="row alur">
            <div class="col-md-6 col-md-offset-3 col-md-pull-3 animate-box fadeInLeft animated" data-animate-effect="fadeInLeft">
                <span class="heading-meta"><i> I-FEST ACTIVITY </i></span>
                <h2 class="colorlib-heading animate-box fadeInUp animated"> Schedule </h2>
            </div>
        </div>
        <div class="row kotak">
            <div class="col-md-12">
                <div class="timeline-centered">
                    <article class="timeline-entry animate-box fadeInLeft animated" data-animate-effect="fadeInLeft">
                    <div class="timeline-entry-inner">
                        <div class="timeline-icon color-1">
                            <b style="color: #fff;"> 01 </b>
                        </div>
                        <div class="timeline-label">
                            <h2 class="activity">
                                <a href="#"><i> Workshop "DATA SCIENCE" </i></a>
                                <br><br>
                                <span> Jumat, 05 Juli 2019 </span>
                            </h2>
                            <p></p>
                        </div>
                    </div>
                    </article>
                    <article class="timeline-entry animate-box fadeInRight animated sampingan" data-animate-effect="fadeInRight">
                    <div class="timeline-entry-inner">
                        <div class="timeline-icon color-2">
                            <b style="color: #fff;"> 02 </b>
                        </div>
                        <div class="timeline-label">
                            <h2 class="activity">
                                <a href="#"><i> I-FEST Competition <br> "Progressive Coding & Mobile Apps UI / UX" </i></a>
                                <br><br>
                                <span> Sabtu, 06 Juli 2019 </span>
                            </h2>
                            <p></p>
                        </div>
                    </div>
                    </article>
                    <article class="timeline-entry animate-box fadeInLeft animated sampingan" data-animate-effect="fadeInLeft">
                    <div class="timeline-entry-inner">
                        <div class="timeline-icon color-3">
                            <b style="color: #fff;"> 03 </b>
                        </div>
                        <div class="timeline-label">
                            <h2 class="activity">
                                <a href="#"><i> Seminar "SPIRIT#9"</i></a>
                                <br><br>
                                <span>Sabtu, 13 Juli 2019</span>
                            </h2>
                            <p></p>
                        </div>
                    </div>
                    </article>
                    <article class="timeline-entry begin animate-box fadeInUp animated" data-animate-effect="fadeInBottom">
                    <div class="timeline-entry-inner">
                        <div class="timeline-icon color-none">
                        </div>
                    </div>
                    </article>
                </div>
            </div>
        </div>
    </div>
</section>
<!-- Modernizr JS -->
<script src="custom/alur/js/modernizr-2.6.2.min.js"></script>