<?php require 'custom/10_footer.html'; ?>
<div class="container-fluid footer">
    <div class="row foot">
        <div class="col-md-6 cp">
            <p class="cp"><small>&copy; Copyright <script>document.write(new Date().getFullYear());</script> I-FEST </a></a></small></p>
        </div>
        <div class="col-md-6 mark">
            <p class="cp-pro"><small> Made with <i class="icon-heart" aria-hidden="true"></i> by <a href="https://instagram.com/akhtarfath_" target="_blank"> Another Part of HIMA TI UHAMKA </a>  </small></p>
        </div>
    </div>
</div>