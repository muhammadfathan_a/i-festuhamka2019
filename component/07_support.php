<?php require("custom/07_support.html"); ?>
<div id="support">
    <div class="title">
        <h1 class="title-support"> SUPPORTED BY </h1>
    </div>
    <div class="container container-left support">
        <div class="row support">
            <div class="cell-td6 cell-dd4 support">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Php-indonesia.png" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 support">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Microsoft-student-partner.png" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 support">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Permikomnas.png" class="gambar-support">
                </div>
            </div>
        </div>
    </div>
    <div class="title">
        <h1 class="title-support"> SPONSORED BY </h1>
    </div>
    <div class="container container-left support">
        <div class="row sponsored">
            <div class="cell-td6 cell-dd4 sponsored">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Smartfren.jpg" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 sponsored">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Bitcoin.png" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 sponsored">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/Qwords.png" class="gambar-support">
                </div>
            </div>
        </div>
    </div>
    <div class="title">
        <h1 class="title-support"> MEDIA PARTNER </h1>
    </div>
    <div class="container container-left support">
        <div class="row media">
            <div class="cell-td6 cell-dd4 media">
                <div class="img-place support">
                    <img  src="assets/landing/img/Support/Event-Ngampus.png" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 media">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/AcaraBaru-INTV.png" class="gambar-support">
                </div>
            </div>
            <div class="cell-td6 cell-dd4 media">
                <div class="img-place support">
                    <img src="assets/landing/img/Support/JKT-Info.png" class="gambar-support">
                </div>
            </div>
        </div>
    </div>
</div>