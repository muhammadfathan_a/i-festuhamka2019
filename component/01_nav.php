<?php require('custom/01_nav.html'); ?>

<nav class="navbar sticky">
	<a href="" class="navbrand">
		<img src="assets/images/logoV2White.png" alt="logo" class="logo">
		<img src="assets/images/logoV2.png" alt="logo" class="logo-web">
	</a>
	<div class="nav-toggle">
		<span class="bar bar1" id="bar1"></span>
		<span class="bar" id="bar2"></span>
		<span class="bar bar2" id="bar3"></span>
	</div>
	<ul class="navmenu right">
		<form method="post">
		<div class="navmenu-btn">
			<li><a href="#competition" class="scroll"> Competition </a></li>
			<li><a href="#workshop" class="scroll"> Workshop </a></li>
			<li><a href="#seminar" class="scroll"> Seminar </a></li>
			<li><button class="scroll btn-nav" name="galeri-btn"> Gallery </button></li>
		</div>
		<div class="navmenu-login">
			<li class="login-btn"><a href="login/" target="_blank" class="login-btn"> Login </a></li>
		</div>
		</form>
	</ul>
</nav>