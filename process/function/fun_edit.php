<?php 

class Edit {
    // Peserta
    public $idPeserta, $nama, $edukasi, $institusi, $telp, $lahir;
    // Team
    public $email, $password, $idTeam, $active;

    // Content
    public $status;

    // Universal
    public $row;

        // Function Peserta Team
        public function editContent($getNama, $getInstitusi, $getEmail, $getNoTelp, $getStatus, $getId, $getQuery) {
                    
            $this->nama      = $getNama;
            $this->institusi = $getInstitusi;
            $this->email     = $getEmail; 
            $this->telp      = $getNoTelp; 
            $this->status    = $getStatus; 
            $this->idPeserta = $getId;

            $this->query    = $getQuery;

                $data[] = $this->nama;
                $data[] = $this->institusi;
                $data[] = $this->email;
                $data[] = $this->telp;
                $data[] = $this->status;
                $data[] = $this->idPeserta;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row;
        }

        // Function Peserta Team
        public function editPeserta($getNama, $getEdukasi, $getInstitusi, $getNoTelp, $getTglLahir, $getId, $getQuery) {
                    
            $this->nama      = $getNama;
            $this->edukasi   = $getEdukasi; 
            $this->institusi = $getInstitusi;
            $this->telp      = $getNoTelp; 
            $this->lahir     = $getTglLahir; 
            $this->idPeserta = $getId;

            $this->query    = $getQuery;

                $data[] = $this->nama;
                $data[] = $this->edukasi;
                $data[] = $this->institusi;
                $data[] = $this->telp;
                $data[] = $this->lahir;
                $data[] = $this->idPeserta;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row;
        }

        public function editTeam($getEmail, $getPassword, $getIdTeam, $getQuery) {

            $this->email = $getEmail;
            $this->password = $getPassword;
            $this->idTeam = $getIdTeam;

            $this->query = $getQuery;

                $data[] = $this->email;
                $data[] = $this->password;
                $data[] = $this->idTeam;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row = $row;
        }


        public function adminTeamEdit($getNama, $getEmail, $getStatus, $getActive, $getIdTeam, $getQuery) {

            $this->nama     = $getNama;
            $this->email    = $getEmail;
            $this->status   = $getStatus;
            $this->active   = $getActive;
            $this->idTeam   = $getIdTeam;

            $this->query = $getQuery;

                $data[] = $this->nama;
                $data[] = $this->email;
                $data[] = $this->status;
                $data[] = $this->active;
                $data[] = $this->idTeam;    

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row = $row;
        }

}