<?php
    
    class Register {

        public $nama, $email, $telp, $instansi, $password;
        public $edukasi, $institusi, $tglLahir, $idTeam;
        public $status;

        public $query;
        public $row;

         // Function Admin Content : Seminar and Workshop
         public function adminRegisterContent($getNama, $getInstansi, $getEmail, $getTelp, $getStatus, $getQuery) {

            $this->nama      = $getNama;
            $this->instansi  = $getInstansi;
            $this->email     = $getEmail;
            $this->telp      = $getTelp;
            $this->status    = $getStatus;

                $this->query     = $getQuery; 
                
                $data[] = $this->nama;
                $data[] = $this->instansi;
                $data[] = $this->telp;
                $data[] = $this->status;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row;
        }

        // Function Content : Seminar and Workshop
        public function registerContent($getNama, $getInstansi, $getEmail, $getTelp, $getQuery) {

            $this->nama      = $getNama;
            $this->instansi  = $getInstansi;
            $this->email     = $getEmail;
            $this->telp      = $getTelp;

            $this->query     = $getQuery; 
                
                $data[] = $this->nama;
                $data[] = $this->instansi;
                $data[] = $this->email;
                $data[] = $this->telp;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row;
        }

        // Function Competition : Progressive Coding and Mobile App UI/UX
        public function registerCompetition($getNama, $getEmail, $getPassword, $getQuery) {
            
            $this->nama     = $getNama;
            $this->email    = $getEmail; 
            $this->password = $getPassword;

            $this->query    = $getQuery;

                $data[] = $this->nama;
                $data[] = $this->email;
                $data[] = $this->password;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;
        
        return $this->row;
        }

        // Function Peserta Team
        public function registerPeserta($getNama, $getEdukasi, $getInstitusi, $getNoTelp, $getTglLahir, $getIdTeam, $getQuery) {
                    
            $this->nama      = $getNama;
            $this->edukasi   = $getEdukasi; 
            $this->institusi = $getInstitusi;
            $this->telp      = $getNoTelp; 
            $this->tgl_lahir = $getTglLahir;
            $this->idTeam    = $getIdTeam; 

            $this->query    = $getQuery;

                $data[] = $this->nama;
                $data[] = $this->edukasi;
                $data[] = $this->institusi;
                $data[] = $this->telp;
                $data[] = $this->tgl_lahir;
                $data[] = $this->idTeam;

            global $conn;

            $row = $conn -> prepare($this->query);
            $row -> execute($data);

            $this->row = $row;

        return $this->row;
        }

    }
?>