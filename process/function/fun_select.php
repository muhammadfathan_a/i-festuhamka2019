<?php

    class Select {

        public $query, $result, $idTeam, $email;

        // Select
        public function dataOnlySelect($getQuery) {
            $this->query  = $getQuery;
            
            global $conn;

                $conn -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                $this->result = $conn-> prepare($this->query);
                $this->result -> execute(array($this->idTeam));

        return $this->result;
        }

        // Select
        public function dataSelect($getIdTeam, $getQuery) {
            $this->idTeam = $getIdTeam;
            $this->query  = $getQuery;
            
            global $conn;

                $conn -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                $this->result = $conn-> prepare($this->query);
                $this->result -> execute(array($this->idTeam));

        return $this->result;
        }
        
        // Select
        public function mailerSelect($getEmail, $getQuery) {
            $this->email = $getEmail;
            $this->query  = $getQuery;
            
            global $conn;

                $conn -> setAttribute( PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION );
                $this->result = $conn-> prepare($this->query);
                $this->result -> execute(array($this->idTeam));

        return $this->result;
        }

    }