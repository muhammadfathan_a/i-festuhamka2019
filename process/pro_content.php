<?php require('system/db_conn.php');
// Workshop
    $workshop = $_POST['regist-workshop'];

    if(isset($workshop)) {
        require('process/function/fun_register.php');
        $Register = new Register();

            $nama     = $_POST['nama_ws'];
            $instansi = $_POST['instansi_ws'];
            $email    = $_POST['email_ws'];
            $telp     = $_POST['telp_ws'];

        require('system/db_query.php');
        $Query = new DBquery();

        $Register->registerContent($nama, $instansi, $email, $telp, $Query->addWorkshop);
        $rowCount = $Register->row -> rowCount();
        
        if($rowCount > 0) {
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL MENDAFTAR!",
                    "WORKSHOP - IT FESTIVAL UHAMKA",
                    "success"
                );
                history.go(-1);
            </script>';
        }    
        else {
            echo 
            '<script>   
                Swal.fire({
                    type: "error",
                    title: "GAGAL MENDAFTAR!",
                    text: "NAMA ANDA SUDAH TERDAFTAR!",
                })
            </script>';
        }
    }   
    else if(isset($_POST['#regist-workshop'])) {
        echo 
        '<script>   
            Swal.fire({
                type: "error",
                title: "GAGAL MENDAFTAR!",
                text: "KUOTA KAMI SUDAH PENUH!",
            })
        </script>';
    }


// Seminar
    $seminar = $_POST['regist-seminar'];

    if(isset($seminar)) {
        require('process/function/fun_register.php');
        $Register = new Register();

            $nama     = $_POST['nama_sm'];
            $instansi = $_POST['instansi_sm'];
            $email    = $_POST['email_sm'];
            $telp     = $_POST['telp_sm'];

        require('system/db_query.php');
        $Query = new DBquery();

        $Register->registerContent($nama, $instansi, $email, $telp, $Query->addSeminar);
        $rowCount = $Register->row -> rowCount();
        
        if($rowCount > 0) {
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL MENDAFTAR!",
                    "SEMINAR - IT FESTIVAL UHAMKA",
                    "success"
                );
                history.go(-1);
            </script>';
        }    
        else {
            echo 
            '<script>   
                Swal.fire({
                    type: "error",
                    title: "GAGAL MENDAFTAR!",
                    text: "NAMA ANDA SUDAH TERDAFTAR!",
                })
            </script>';
        }

    }   
    else if(isset($_POST['#regist-seminar'])) {
        echo 
        '<script>   
            Swal.fire({
                type: "error",
                title: "GAGAL MENDAFTAR!",
                text: "KUOTA KAMI SUDAH PENUH!",
            })
        </script>';
    }


?>