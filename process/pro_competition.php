<?php

use PHPMailer\PHPMailer\PHPMailer;

require('process/function/fun_register.php');
require('system/db_conn.php');

$Register   = new Register();

// Progressive Coding!
    $progressiveCode = $_POST['regist_pc'];
    
    if(isset($progressiveCode)) {
        
            $nama     = $_POST['nama_team_pc'];
            $email    = $_POST['email_pc'];
            $password = $_POST['password_pc'];

        require('system/db_query.php');
        $Query      = new DBquery();

        $Register->registerCompetition($nama, $email, $password, $Query->addPC);
        $rowCount = $Register->row -> rowCount(); 
            
        if($rowCount > 0) {
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL MENDAFTAR LOMBA!",
                    "PROGRESSIVE CODING - IT FESTIVAL UHAMKA",
                    "success"
                );
                window.location = "login/";
            </script>';
        }    
        else {
            echo 
            '<script>   
                Swal.fire({
                    type: "error",
                    title:"GAGAL MENDAFTAR!",
                    text: "TEAM ANDA SUDAH TERDAFTAR",
                })
            </script>';
        }
    }


// Mobile App
$mobileApp = $_POST['regist_ma'];
    
if(isset($mobileApp)) {

        $nama     = $_POST['nama_team_ma'];
        $email    = $_POST['email_ma'];
        $password = $_POST['password_ma']; 

        require('system/db_query.php');
        $Query      = new DBquery();

    $Register->registerCompetition($nama, $email, $password, $Query->addMA);
    $rowCount = $Register->row -> rowCount();

    if($rowCount > 0) {
        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENDAFTAR LOMBA!",
                "MOBILE APP UI/UX - IT FESTIVAL UHAMKA",
                "success"
            );
            window.location = "login/";
        </script>';
    }    
    else {
        echo 
        '<script>   
            Swal.fire({
                type: "error",
                title:"GAGAL MENDAFTAR!",
                text: "NAMA ANDA SUDAH TERDAFTAR",
            })
        </script>';
    }
}

?>