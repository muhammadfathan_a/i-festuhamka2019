<?php

$login = $_POST['login'];

    if(isset($login)) { 
        
        $username = $_POST['username']; $data[] = $username;
        $password = $_POST['password']; $data[] = $password;

        require('../system/db_conn.php'); 
        require('../system/db_query.php');
        
            $Query = new DBquery();

            $row = $conn -> prepare($Query->SelectPeserta);
            $row -> execute($data);
            $rowCount1 = $row -> rowCount();

        if($rowCount1 > 0) {  
            $result = $row -> fetch();
            $_SESSION['ifest_peserta'] = $result;
            
            if($_SESSION['ifest_peserta']['active'] == 'AKTIF') {
                echo 
                '<script>
                    Swal.fire(
                        "SELAMAT DATANG!",
                        "PESERTA - IT FESTIVAL UHAMKA ",
                        "success"
                    );
                    window.location = "../panel/";
                </script>';
            }
            else {
            UNSET($_SESSION['ifest_peserta']);
            echo 
            '<script>   
                Swal.fire({
                    type: "error",
                    title: "LOGIN GAGAL!",
                    text: "AKUN ANDA BELUM AKTIF, PERIKSA EMAIL ANDA!",
                });
            </script>';
            }
        }
        else { 
            $row = $conn -> prepare($Query->SelectAdmin);
            $row -> execute($data);
            $rowCount2 = $row -> rowCount();

            if($rowCount2 > 0) {
                $result = $row -> fetch();
                $_SESSION['ifest_admin'] = $result;
                echo 
                '<script>    
                    Swal.fire(
                        "SELAMAT DATANG!",
                        "ADMIN - IT FESTIVAL UHAMKA ",
                        "success"
                    );      
                    window.location = "../panel/";
                </script>';
            } else {
            UNSET($_SESSION['ifest_admin']);
            echo 
            '<script>   
                Swal.fire({
                    type: "error",
                    title: "LOGIN GAGAL!",
                    text: "PERIKSA USERNAME / PASSWORD ANDA",
                })
            </script>';
            }
        }
        echo 
        '<script>
            window.location: "../login/";
        </script>';
    }

?>