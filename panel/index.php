<?php session_start();

    if($_SESSION['ifest_peserta']) {
        header('Location: peserta/');
    }
    else if($_SESSION['ifest_admin']) {
        header('Location: admin/');
    }
    else {
        header('Location: ../login/');
    }

?>