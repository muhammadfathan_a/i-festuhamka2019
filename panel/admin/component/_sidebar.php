<?php require('custom/_sidebar.html');
?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
<ul class="nav">
    <li class="nav-item nav-profile">
    <form method="post">
    <div class="nav-link">
    <button name="manage-team" class="manage-team">
        <div class="user-wrapper">
        <div class="profile-image">
            <img src="../assets/images/team/team1.png" alt="profile image">
        </div>
        <div class="text-wrapper">
            <p class="profile-name"><?= $_SESSION['ifest_admin']['nama_admin']; ?></p>
            <div>
            <small class="designation text-muted"><?= $_SESSION['ifest_admin']['username']; ?></small>
                <span class="status-indicator online"></span>
            </div>
        </div>
        </div>
    </button>
    </div>
    </form>
    </li>
    <form method="post">
    <li class="nav-item">
    <a class="nav-link" href="../">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title"> Dashboard
        </span>
    </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"> 
        <i class="menu-icon mdi mdi-lava-lamp"></i> 
            <button class="update-seminar-btn" name="seminar"> Seminar </button>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"> 
        <i class="menu-icon mdi mdi-security-network"></i> 
            <button class="update-workshop-btn" name="workshop"> Workshop </button>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"> 
        <i class="menu-icon mdi mdi-trophy-outline"></i> 
            <button class="update-competition-btn" name="competition"> Lomba </button>
        </a>
    </li>
    <li class="nav-item">
    <a class="nav-link" aria-expanded="false" aria-controls="ui-basic">
        <i class="menu-icon mdi mdi-content-copy"></i>
        <span class="menu-title">
            <button class="update-content-btn" name="content"> Konten Website </button>
        </span>
    </a>
    </li>
    <li class="nav-item"> 
        <hr class="pembatas">
    </li>
    <li class="nav-item mail">
    <a class="nav-link" data-toggle="collapse" href="#ui-basic" aria-expanded="true" aria-controls="ui-basic">
        <i class="menu-icon mdi mdi-email"></i>
        <span class="menu-title"> Kirim Email </span>
        <i class="menu-arrow"></i>
    </a>
    <div class="collapse show" id="ui-basic" style="">
        <ul class="nav flex-column sub-menu">
            <li class="nav-item">
                <a class="nav-link" aria-expanded="false" aria-controls="ui-basic">
                    <span class="menu-title">
                        <button class="update-content-btn" name="email"> Email Broadcast </button>
                    </span>
                </a>
            </li>
        </ul>
    </div>
    </li>
    </form>
</ul>
</nav>