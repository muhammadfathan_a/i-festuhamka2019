<link rel="shortcut icon" type="text/icon" href="../../assets/images/faviconV3.ico">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php session_start(); error_reporting(1);
    
    require('../../system/db_conn.php');
    require('../../system/db_query.php');

    require('../../process/function/fun_register.php');
    require('../../process/function/fun_edit.php');
    require('../../process/function/fun_delete.php');
    require('../../process/function/fun_select.php');

      $Register = new Register();
      $Edit     = new Edit();
      $Delete   = new Delete();
      $Query    = new DBquery();
      $Select   = new Select();

        $dashboard     = $_POST['dashboard'];
        $seminar       = $_POST['seminar'];
        $workshop      = $_POST['workshop'];
        $competition   = $_POST['competition'];
        $content       = $_POST['content'];
        $email         = $_POST['email'];
      
    if(!$_SESSION['ifest_peserta'] && !$_SESSION['ifest_admin']) {
        echo '<script> window.location = "../../login/"; </script>';
    }
    include "library/_lib-css.php"; 
?>
<body>
  <div class="container-scroller">
    <?php include "component/_navbar.php"; ?>
      <div class="container-fluid page-body-wrapper">
      <?php include "component/_sidebar.php"; ?>
      <div class="main-panel">
        <div class="content-wrapper">
        <?php // include "_popup-message.php"; ?>
          <?php 

                if(isset($seminar) || $_GET['es']) {
                  echo "<title> Seminar | IFEST </title>";
                  include "pages/_seminar.php";
                } 
                else if(isset($workshop) || $_GET['ew']) {
                  echo "<title> Workshop | IFEST </title>";
                  include "pages/_workshop.php";
                } 
                else if(isset($competition) || $_GET['et']) {
                  echo "<title> Competition | IFEST </title>";
                  include "pages/_competition.php";
                }
                else if(isset($email)) {
                  echo "<title> Broadcast Email | IFEST </title>";
                  include "pages/_email.php";
                }
                // else if(isset($content) || $_GET['econ']) {
                  // echo "<title> Content | IFEST </title>";
                  // include "pages/_content.php";
                // }
                else {
                  echo "<title> Dashboard | IFEST </title>";
                  include "pages/_dashboard.php";
                }
                
          ?>
        <!-- close of content-wrapper -->
        </div>
        <?php include "component/_footer.php"; ?>
      <!-- close of main-panel -->
      </div>
    <!-- close of container-fluid page-body-wrapper -->
    </div>
  <!-- close of container-scroller -->
  </div>
</body>
<?php include "library/_lib-js.php"; ?>
<?php include "process/pro_delete.php"; ?>
<noscript> 
  <style>
    * {
      display: none;
    }
  </style>
</noscript>