<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<!-- plugins:js -->
<script src="../assets/vendors/js/vendor.bundle.base.js"></script>
<script src="../assets/vendors/js/vendor.bundle.addons.js"></script>
<!-- endinject -->
<!-- Plugin js for this page-->
<!-- End plugin js for this page-->
<!-- inject:js -->
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="../assets/js/off-canvas.js"></script>
<script src="../assets/js/misc.js"></script>
<!-- endinject -->
<!-- Custom js for this page-->
<script src="assets/js/dashboard.js"></script>
<!-- End custom js for this page-->


<!-- jQuery -->
<script src="../assets/js/jquery.min.js"></script>
<!-- jQuery Easing -->
<script src="../assets/js/jquery.easing.1.3.js"></script>
<!-- Bootstrap -->
<script src="../assets/js/bootstrap.min.js"></script>
<!-- Waypoints -->
<script src="../assets/js/jquery.waypoints.min.js"></script>
<!-- Flexslider -->
<script src="../assets/js/jquery.flexslider-min.js"></script>
<!-- Owl carousel -->
<script src="../assets/js/owl.carousel.min.js"></script>
<!-- Counters -->
<script src="../assets/js/jquery.countTo.js"></script>


<!-- MAIN JS -->
<script src="../assets/js/main.js"></script>
