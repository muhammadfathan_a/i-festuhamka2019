<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<!-- Include Editor style. -->
<link href='https://cdn.jsdelivr.net/npm/froala-editor@3.0.0/css/froala_editor.pkgd.min.css' rel='stylesheet' type='text/css' />

<!-- Include JS file. -->
<script type='text/javascript' src='https://cdn.jsdelivr.net/npm/froala-editor@3.0.0/js/froala_editor.pkgd.min.js'></script>

<link rel="stylesheet" href="../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.addons.css">

<link rel="stylesheet" href="../assets/css/style.css">

<link href="https://fonts.googleapis.com/css?family=Quicksand:300,400,500,700" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Playfair+Display:400,400i,700" rel="stylesheet">

<!-- Animate.css -->
<link rel="stylesheet" href="../assets/css/animate.css">
<!-- Icomoon Icon Fonts-->
<link rel="stylesheet" href="../assets/css/icomoon.css">
<!-- Flexslider  -->
<link rel="stylesheet" href="../assets/css/flexslider.css">
<!-- Flaticons  -->
<link rel="stylesheet" href="../assets/fonts/flaticon/font/flaticon.css">
<!-- Owl Carousel -->
<link rel="stylesheet" href="../assets/css/owl.carousel.min.css">
<link rel="stylesheet" href="../assets/css/owl.theme.default.min.css">
<!-- Theme style  -->
<link rel="stylesheet" href="../assets/css/style.css">

<link rel="shortcut icon" href="../assets/images/favicon.png" />

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="assets/js/jquery-3.4.1.min.js"></script>
<!-- Modernizr JS -->
<script src="../assets/js/modernizr-2.6.2.min.js"></script>