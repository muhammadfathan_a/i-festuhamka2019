<p class="card-description">
Update competition team
</p>
<div id="editAnggota">
<form method="post">
    <div class="input-group">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-account text-white"></i>
        </span>
        </div>
        <input name="nama_team" type="text" class="form-control" placeholder="Nama lengkap" required="required"
        value="<?= $row['nama_team']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-email-open-outline text-white"></i>
        </span>
        </div>
        <input name="email" type="email" class="form-control" placeholder="Email" required="required"
        value="<?= $row['email']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-account-check text-white"></i>
        </span>
        </div>
        <select name="status" class="form-control">
        <option>LUNAS</option>
        <option>BELUM LUNAS</option>
        </select>
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-account-check text-white"></i>
        </span>
        </div>
        <select name="active" class="form-control">
        <option>AKTIF</option>
        <option>BELUM AKTIF</option>
        </select>
    </div>
    <div class="input-group mt-2">
        <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
        <button id="btn-tambah-anggota" name="team-edit"> Update Team </button>
    </div>
</form>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php

        global $afterReplace2;
        $edit = $_POST['team-edit'];

            $id         = $afterReplace2;
            $nama       = $_POST['nama_team'];
            $email      = $_POST['email'];
            $status     = $_POST['status'];
            $active     = $_POST['active'];

        if(isset($edit)) {
            $Edit->adminTeamEdit($nama, $email, $status, $active, $id, $Query->adminTeamEdit);
            
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL UBAH TEAM!",
                    "IFEST - COMPETITION",
                    "success"
                );
                window.location = "../";
            </script>';   
                 
        }
  

?>