<p class="card-description">
Update peserta seminar
</p>
<div id="editAnggota">
<form method="post">
    <div class="input-group">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-account text-white"></i>
        </span>
        </div>
        <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama lengkap" required="required"
        value="<?= $row['nama']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-city text-white"></i>
        </span>
        </div>
        <input name="instansi" type="text" class="form-control" placeholder="Instansi" required="required"
        value="<?= $row['instansi']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-email-open-outline text-white"></i>
        </span>
        </div>
        <input name="email" type="email" class="form-control" placeholder="Email" required="required"
        value="<?= $row['email']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-phone text-white"></i>
        </span>
        </div>
        <input name="telp" type="number" class="form-control" placeholder="Nomor telepon" required="required"
        value="<?= $row['telp']; ?>">
    </div>
    <div class="input-group mt-2">
        <div class="input-group-prepend bg-warning">
        <span class="input-group-text bg-transparent">
            <i class="mdi mdi-account-check text-white"></i>
        </span>
        </div>
        <select name="status" class="form-control">
        <option>LUNAS</option>
        <option>BELUM LUNAS</option>
        </select>
    </div>
    <div class="input-group mt-2" style="display: none;">
        <div class="input-group-prepend bg-warning">
            <span class="input-group-text bg-transparent">
                <i class="mdi mdi-shield text-white"></i>
            </span>
        </div>
        <input  name="id_peserta"
                type="number" 
                class="form-control" 
                placeholder="No team" 
                value="<?= $row['id_peserta']; ?>" 
        >
    </div>
    <div class="input-group mt-2">
        <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
        <button id="btn-tambah-anggota" name="peserta-edit"> Update Peserta </button>
    </div>
</form>
</div>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php

        global $afterReplace2;
        $edit = $_POST['peserta-edit'];

            $id         = $afterReplace2;
            $nama       = $_POST['nama_lengkap'];
            $instansi   = $_POST['instansi'];
            $email      = $_POST['email'];
            $telp       = $_POST['telp'];
            $status     = $_POST['status'];

        if(isset($edit)) {
            $Edit->editContent($nama, $instansi, $email, $telp, $status, $id, $Query->editWorkshop);
            
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL UBAH PESERTA!",
                    "IFEST - WORKSHOP",
                    "success"
                );
                window.location = "../";
            </script>';   
                 
        }
  

?>