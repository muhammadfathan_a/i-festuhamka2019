<?php 
    require('custom/_email.html');
    require('custom/_anggota.html'); 
?>
<div class="row">
    <div class="col-md-12 animate-box fadeInLeft animated" data-animate-effect="fadeInLeft">
        <div class="fancy-collapse-panel">
            <div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingOne">
                        <h4 class="panel-title">
                            <a data-toggle="collapse" data-parent="#accordion" href="#collapseOne" aria-expanded="false" aria-controls="collapseOne" class="collapsed"> Seminar
                            </a>
                        </h4>
                    </div>
                    <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="tambahAnggota">
                                        <div class="form-group">
                                        <p class="card-description">
                                            <b> Broadcast ke Peserta: </b>
                                        </p>
                                        <form method="post" action="process/pro_email.php">
                                        <div class="input-group">
                                            <span>
                                                <input name="broadcast-to" id="belum-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="belum-bayar"> Belum Bayar </label>
                                            </span>
                                            <span>
                                                <input name="broadcast-to" id="sudah-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="sudah-bayar"> Sudah Bayar </label>
                                            </span>
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-account text-white"></i>
                                            </span>
                                            </div>
                                            <input name="subjek" type="text" class="form-control" placeholder="Subjek email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-email-open-outline text-white"></i>
                                            </span>
                                            </div>
                                            <input name="email" type="email" class="form-control" placeholder="Isi Email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                                            <button id="btn-tambah-anggota" name="anggota-submit"> Kirim Email </button>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingTwo">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo"> Workshop
                            </a>
                        </h4>
                    </div>
                    <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingTwo" aria-expanded="false" style="height: 0px;">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="tambahAnggota">
                                        <div class="form-group">
                                        <p class="card-description">
                                            <b> Broadcast ke Peserta: </b>
                                        </p>
                                        <form method="post" action="process/pro_email.php">
                                        <div class="input-group">
                                            <span>
                                                <input name="broadcast-to" id="belum-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="belum-bayar"> Belum Bayar </label>
                                            </span>
                                            <span>
                                                <input name="broadcast-to" id="sudah-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="sudah-bayar"> Sudah Bayar </label>
                                            </span>
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-account text-white"></i>
                                            </span>
                                            </div>
                                            <input name="subjek" type="text" class="form-control" placeholder="Subjek email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-email-open-outline text-white"></i>
                                            </span>
                                            </div>
                                            <input name="email" type="email" class="form-control" placeholder="Isi Email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                                            <button id="btn-tambah-anggota" name="anggota-submit"> Kirim Email </button>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="panel panel-default">
                    <div class="panel-heading" role="tab" id="headingThree">
                        <h4 class="panel-title">
                            <a class="collapsed" data-toggle="collapse" data-parent="#accordion" href="#collapseThree" aria-expanded="false" aria-controls="collapseThree"> Lomba
                            </a>
                        </h4>
                    </div>
                    <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingThree" aria-expanded="false">
                        <div class="panel-body">
                            <div class="row">
                                <div class="col-md-12">
                                    <div id="tambahAnggota">
                                        <div class="form-group">
                                        <p class="card-description">
                                            <b> Broadcast ke Peserta: </b>
                                        </p>
                                        <form method="post" action="process/pro_email.php">
                                        <div class="input-group">
                                            <span>
                                                <input name="broadcast-to" id="belum-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="belum-bayar"> Belum Bayar </label>
                                            </span>
                                            <span>
                                                <input name="broadcast-to" id="sudah-bayar" type="radio" class="radio-mail" required="required"> 
                                                <label class="labale" for="sudah-bayar"> Sudah Bayar </label>
                                            </span>
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-account text-white"></i>
                                            </span>
                                            </div>
                                            <input name="subjek" type="text" class="form-control" placeholder="Subjek email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <div class="input-group-prepend bg-warning">
                                            <span class="input-group-text bg-transparent">
                                                <i class="mdi mdi-email-open-outline text-white"></i>
                                            </span>
                                            </div>
                                            <input name="email" type="email" class="form-control" placeholder="Isi Email" required="required">
                                        </div>
                                        <div class="input-group mt-2">
                                            <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                                            <button id="btn-tambah-anggota" name="anggota-submit"> Kirim Email </button>
                                        </div>
                                        </form>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<div class="fluid-container">
    <div class="row">
        <div class="col-md-12">
            <h1 class="ml-2 mt-5 mb-2 text-center"> Contoh Email Broadcast </h1>
        </div>
        <div class="col-md-12">
            <div class="message" style="width: 600px; margin: 25px auto; border: 2px solid #74a1d8;">
                <div class="head-message" style="margin: 0 auto;">
                    <h2 style="margin: 0 auto; text-align: center; background: #74a1d8; padding: 20px; color: #fff;font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif"> 
                        I-FEST UHAMKA 2019 
                    </h2>
                </div>
                <div class="body-message" style="color: #fff; background-image: url(../assets/images/mail.png); background-size: 100%; background-position: center;">
                    <p style="padding-top: 25px; font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; text-align: center; font-size: 16px;">
                        <b> Muhammad Fathan Aulia <br> Universitas Muhammadiyah Prof. Dr. HAMKA </b>
                    </p>
                    <p style="padding-top: 25px; font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; text-align: center;">
                        PESERTA - WORKSHOP <br> <i>"Improve The Experience With Data Science"</i>
                    </p>
                    <p style="margin: 0 auto; font-family:'Trebuchet MS', 'Lucida Sans Unicode', 'Lucida Grande', 'Lucida Sans', Arial, sans-serif; text-align: center; padding-bottom: 30px;">
                        <b> Hari / Tanggal : Jumat, 05 Juli 2019 </b> 
                        <br>
                        <b> Tempat : Fakultas Teknik UHAMKA </b>
                    </p>
                </div>
            </div>
        </div>
    </div>
</div>