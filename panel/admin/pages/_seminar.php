<?php 
  require('custom/_anggota.html'); ?>
  <div class="anggota">
  <div class="container-scroller">
    <!-- partial -->
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                <h1 class="card-title"> > Seminar! </h1>
                  <p class="card-description">
                    Manage : 
                    <code> <b> Peserta Seminar Belum Lunas </b> </code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered" style="text-transform: uppercase;">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Opsi </th>
                          <th> Nama Lengkap </th>
                          <th> Instansi </th>
                          <th> Email </th>
                          <th> Telp </th>
                          <th> Status </th>
                          <th> Daftar </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $Select->dataOnlySelect($Query->DataSeminarBlmLunas);
                            $no = 1; 
                            
                            while($row = $Select->result -> fetch()) {            

                              if($row['status'] == 'LUNAS') {
                                $style = "color: green;";
                              }
                              else {
                                $style = "color: red";
                              }
               
                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td class="no"><?= $no; ?></td>
                          <td>
                            <div class="row btn">
                              <div class="col-sm-12">
                                <a href="?es=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-ubah"> 
                                  Ubah
                                </a>
                                <span class="gap"> | </span>
                                <a href="?ds=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-hapus"> 
                                  Hapus 
                                </a>
                              </div>
                            </div>
                          </td>
                          <td><b><?= $row['nama']; ?></b></td>
                          <td><?= $row['instansi']; ?></td>
                          <td><?= $row['email']; ?></td>
                          <td><?= $row['telp']; ?></td>
                          <td style="<?= $style; ?>"> <b> <?= $row['status']; ?> </b> </td>
                          <td><?= $row['waktu_daftar']; ?></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_peserta']; ?>"></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                  <p class="card-description">
                    <span class="kiri"> Manage : 
                      <code> <b> Peserta Seminar Sudah Lunas </b> </code>
                    </span>
                    <span class="kanan">
                      
                    </span>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered" style="text-transform: uppercase;">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Opsi </th>
                          <th> Nama Lengkap </th>
                          <th> Instansi </th>
                          <th> Email </th>
                          <th> Telp </th>
                          <th> Status </th>
                          <th> Daftar </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $Select->dataOnlySelect($Query->DataSeminarLunas);
                            $no = 1; 
                            
                            while($row = $Select->result -> fetch()) {            

                              if($row['status'] == 'LUNAS') {
                                $style = "color: green;";
                              }
                              else {
                                $style = "color: red";
                              }
               
                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td class="no"><?= $no; ?></td>
                          <td>
                            <div class="row btn">
                              <div class="col-sm-12">
                                <a href="?es=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-ubah"> 
                                  Ubah
                                </a>
                                <span class="gap"> | </span>
                                <a href="?ds=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-hapus"> 
                                  Hapus 
                                </a>
                              </div>
                            </div>
                          </td>
                          <td><b><?= $row['nama']; ?></b></td>
                          <td><?= $row['instansi']; ?></td>
                          <td><?= $row['email']; ?></td>
                          <td><?= $row['telp']; ?></td>
                          <td style="<?= $style; ?>"> <b> <?= $row['status']; ?> </b> </td>
                          <td><?= $row['waktu_daftar']; ?></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_peserta']; ?>"></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 grid-margin stretch-card" id="card-form">
                <div class="card">
                <div class="card-body">
                    <?php $Query = new DBquery();

                        if($_GET['es']) {

                          $firstReplace = "1E8A4ZB10";
                          $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

                          $afterReplace1 = str_replace($firstReplace, "", $_GET['es']);
                          $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

                            $Select->dataSelect($afterReplace2, $Query->DataSeminarDoang);
                          
                          while($row = $Select->result -> fetch()) {    
                            
                            require('edit/edit_seminar.php');
                          
                          }

                        } else {
                    ?>
                    <div id="tambahAnggota">
                    <div class="form-group">
                    <p class="card-description">
                      Add peserta seminar baru
                    </p>
                    <form method="post" action="process/pro_seminar.php">
                      <div class="input-group">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-account text-white"></i>
                          </span>
                          </div>
                          <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama lengkap" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-city text-white"></i>
                          </span>
                          </div>
                          <input name="instansi" type="text" class="form-control" placeholder="Instansi" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-email-open-outline text-white"></i>
                          </span>
                          </div>
                          <input name="email" type="email" class="form-control" placeholder="Email" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-phone text-white"></i>
                          </span>
                          </div>
                          <input name="telp" type="number" class="form-control" placeholder="Nomor telepon" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-account-check text-white"></i>
                          </span>
                          </div>
                          <select name="status" class="form-control">
                            <option>LUNAS</option>
                            <option>BELUM LUNAS</option>
                          </select>
                      </div>
                      <div class="input-group mt-2">
                          <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                          <button id="btn-tambah-anggota" name="anggota-submit"> Tambah Peserta </button>
                      </div>
                    <? } ?>
                    </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
          </div>
        </div>
    <!-- page-body-wrapper ends -->
  </div>
</div>
<!-- <script>
  var trDataPeserta = document.getElementById("tr-3");
  
  if(trDataPeserta) {
    document.getElementById("tambahAnggota").style.display = "none";
    document.getElementById("btn-tambah-anggota").setAttribute("disabled","disabled");
  }
</script>
<script>
  $('#card-form').hide();
  
  $('.btn-tambah-anggota').click(function(){
    $('#card-form').show();
  });
</script> -->