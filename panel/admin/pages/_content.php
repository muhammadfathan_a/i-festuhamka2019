<?php 
  require('custom/_anggota.html'); ?>
  <div class="anggota">
  <div class="container-scroller">
    <!-- partial -->
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                <h1 class="card-title"> <?= $_SESSION['ifest_peserta']['nama_team'] ?>! </h1>
                  <p class="card-description">
                    Build up ur
                    <code> <b> best team </b> </code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Nama Lengkap </th>
                          <th> Edukasi </th>
                          <th> Institusi </th>
                          <th> Nomor Telp </th>
                          <th> Tanggal Lahir </th>
                          <th> Opsi </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $Select->dataSelect($_SESSION['ifest_peserta']['id_team'] ,$Query->DataPeserta);
                            $no = 1; 
                            
                            while($row = $Select->result -> fetch()) {                             
                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td><?= $no; ?></td>
                          <td><?= $row['nama_peserta']; ?></td>
                          <td><?= $row['edukasi']; ?></td>
                          <td><?= $row['institusi']; ?></td>
                          <td><?= $row['no_telp']; ?></td>
                          <td><?= $row['tgl_lahir']; ?></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_peserta']; ?>"></td>
                          <td>
                            <div class="row">
                              <div class="col-sm-12">
                                <a href="?y=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-ubah"> 
                                  Ubah
                                </a>
                                <span class="gap"> | </span>
                                <a href="?x=1E8A4ZB10<?= $row['id_peserta']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-hapus"> 
                                  Hapus 
                                </a>
                              </div>
                            </div>
                          </td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 grid-margin stretch-card" id="card-form">
                <div class="card">
                <div class="card-body">
                    <?php $Query = new DBquery();

                        if($_GET['y']) {

                          $firstReplace = "1E8A4ZB10";
                          $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

                          $afterReplace1 = str_replace($firstReplace, "", $_GET['y']);
                          $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

                            $Select->dataSelect($afterReplace2, $Query->DataPesertaDoang);
                          
                          while($row = $Select->result -> fetch()) {    
                            
                            require('edit/edit_anggota.php');
                          
                          }
                        } else {
                    ?>
                    <div id="tambahAnggota">
                    <div class="form-group">
                    <p class="card-description">
                    Input ur team member
                    </p>
                    <form method="post" action="process/pro_anggota.php">
                    <div class="input-group">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-account text-white"></i>
                          </span>
                          </div>
                          <input name="nama_lengkap" type="text" class="form-control" placeholder="Nama lengkap" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-school text-white"></i>
                          </span>
                          </div>
                          <select name="edukasi" class="form-control">
                            <option> Sekolah Menengah Pertama </option>
                            <option> Sekolah Menengah Atas </option>
                            <option> Mahasiswa </option>
                            <option> Profesional </option>
                          </select>
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-city text-white"></i>
                          </span>
                          </div>
                          <input name="institusi" type="text" class="form-control" placeholder="Institusi" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-phone text-white"></i>
                          </span>
                          </div>
                          <input name="telp" type="number" class="form-control" placeholder="Nomor telepon" required="required">
                      </div>
                      <div class="input-group mt-2">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-calendar text-white"></i>
                          </span>
                          </div>
                          <input name="tgl_lahir" type="date" class="form-control" placeholder="Tanggal lahir" required="required">
                      </div>
                      <div class="input-group mt-2" style="display: none;">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-shield text-white"></i>
                          </span>
                          </div>
                          <input  name="id_team"
                                  type="number" 
                                  class="form-control" 
                                  placeholder="No team" 
                                  value="<?= $_SESSION['ifest_peserta']['id_team']; ?>" 
                          >
                      </div>
                      <div class="input-group mt-2">
                          <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                          <button id="btn-tambah-anggota" name="anggota-submit"> TAMBAH ANGGOTA </button>
                      </div>
                    <? } ?>
                    </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
          </div>
        </div>
    <!-- page-body-wrapper ends -->
  </div>
</div>
<script>
  var trDataPeserta = document.getElementById("tr-3");
  
  if(trDataPeserta) {
    document.getElementById("tambahAnggota").style.display = "none";
    document.getElementById("btn-tambah-anggota").setAttribute("disabled","disabled");
  }
</script>
<!-- <script>
  $('#card-form').hide();
  
  $('.btn-tambah-anggota').click(function(){
    $('#card-form').show();
  });
</script> -->