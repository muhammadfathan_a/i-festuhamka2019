<?php 
  require('custom/_anggota.html'); ?>
  <div class="anggota">
  <div class="container-scroller">
    <!-- partial -->
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                <h1 class="card-title"> > Competition! </h1>
                  <p class="card-description">
                    General 
                    <code> <b> "Team Lomba!" </b> </code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Opsi </th>
                          <th> Nama Team </th>
                          <th> Email Team </th>
                          <th> Password Team </th>
                          <th> Kategori Lomba </th>
                          <th> Pembayaran </th>
                          <th> Status </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php

                            $Select->dataOnlySelect($Query->onlyDataTeam);
                              $no = 1;
                              while($row = $Select->result -> fetch()) {

                                  if($row['status'] == 'LUNAS') {
                                    $style = "color: green;";
                                  }
                                  else {
                                    $style = "color: red";
                                  }

                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td class="no"><?= $no; ?></td>
                          <td>
                            <div class="row btn">
                              <div class="col-sm-12">
                                <a href="?et=1E8A4ZB10<?= $row['id_team']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-ubah"> 
                                  Ubah
                                </a>
                                <span class="gap"> | </span>
                                <a href="?dt=1E8A4ZB10<?= $row['id_team']; ?>22D1131412555G1L78ZZHHAQEU" class="btn-hapus"> 
                                  Hapus 
                                </a>
                              </div>
                            </div>
                          </td>
                          <td> <b> <?= $row['nama_team']; ?> </b> </td>
                          <td><?= $row['email']; ?></td>
                          <td><?= $row['password']; ?></td>
                          <td><?= $row['nama_kategori']; ?></td>
                          <td style="<?= $style; ?>">
                            <b> <?= $row['status']; ?> </b>
                          </td>
                          <td><b><?= $row['active']; ?></b></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_team']; ?>"></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                  <p class="card-description ma">
                    Peserta 
                    <code> <b> "Progressive Coding!" </b> </code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Nama Lengkap </th>
                          <th> Nama Team </th>
                          <th> Edukasi </th>
                          <th> Instansi </th>
                          <th> Nomor Telepon </th>
                          <th> Tanggal Lahir </th>
                          <th> Umur </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $Select->dataOnlySelect($Query->onlyDataPC);
                            $no = 1; 
                            
                            while($row = $Select->result -> fetch()) {      

                              $birthday = $row['tgl_lahir'];
	
                              // Convert Ke Date Time
                              $biday = new DateTime($birthday);
                              $today = new DateTime();
                              
                              $diff = $today->diff($biday);
                      
                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td class="no"><?= $no; ?></td>
                          <td><?= $row['nama_peserta']; ?></td>
                          <td> <b> <?= $row['nama_team']; ?> </b> </td>
                          <td><?= $row['edukasi']; ?></td>
                          <td><?= $row['institusi']; ?></td>
                          <td><?= $row['no_telp']; ?></td>
                          <td><?= $row['tgl_lahir']; ?></td>
                          <td><b><?= $diff->y; ?></b></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_peserta']; ?>"></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                  <p class="card-description ma">
                    Peserta
                    <code> <b> "Mobile App UI / UX!" </b> </code>
                  </p>
                  <div class="table-responsive">
                    <table class="table table-bordered">
                      <thead>
                        <tr align="center" class="table-primary">
                          <th> # </th>
                          <th> Nama Lengkap </th>
                          <th> Nama Team </th>
                          <th> Edukasi </th>
                          <th> Instansi </th>
                          <th> Nomor Telepon </th>
                          <th> Tanggal Lahir </th>
                          <th> Umur </th>
                        </tr>
                      </thead>
                      <tbody>
                        <?php
                            $Select->dataOnlySelect($Query->onlyDataMA);
                            $no = 1; 
                            
                            while($row = $Select->result -> fetch()) {      

                              $birthday = $row['tgl_lahir'];
	
                              // Convert Ke Date Time
                              $biday = new DateTime($birthday);
                              $today = new DateTime();
                              
                              $diff = $today->diff($biday);                         
                        ?>
                        <tr id="tr-<?= $no; ?>">
                          <td class="no"><?= $no; ?></td>
                          <td><?= $row['nama_peserta']; ?></td>
                          <td> <b> <?= $row['nama_team']; ?> </b> </td>
                          <td><?= $row['edukasi']; ?></td>
                          <td><?= $row['institusi']; ?></td>
                          <td><?= $row['no_telp']; ?></td>
                          <td><?= $row['tgl_lahir']; ?></td>
                          <td><b><?= $diff->y; ?></b></td>
                          <td style="display: none;"><input name="id_peserta" type="text" value="<?= $row['id_peserta']; ?>"></td>
                        </tr>
                        <?php $no++; } ?>
                      </tbody>
                    </table>
                  </div>
                </div>
              </div>
            </div>
            <div class="col-12 grid-margin stretch-card" id="card-form">
                <div class="card">
                <div class="card-body">
                    <?php $Query = new DBquery();

                        if($_GET['et']) {

                          $firstReplace = "1E8A4ZB10";
                          $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

                          $afterReplace1 = str_replace($firstReplace, "", $_GET['et']);
                          $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

                            $Select->dataSelect($afterReplace2, $Query->onlyDataTeamById);
                          
                          while($row = $Select->result -> fetch()) {    
                            
                            require('edit/edit_competition.php');
                          
                          }

                        }
                    ?>
                </div>
                </div>
            </div>
          </div>
        </div>
    <!-- page-body-wrapper ends -->
  </div>
</div>
<!-- <script>
  var trDataPeserta = document.getElementById("tr-3");
  
  if(trDataPeserta) {
    document.getElementById("tambahAnggota").style.display = "none";
    document.getElementById("btn-tambah-anggota").setAttribute("disabled","disabled");
  }
</script>
<script>
  $('#card-form').hide();
  
  $('.btn-tambah-anggota').click(function(){
    $('#card-form').show();
  });
</script> -->