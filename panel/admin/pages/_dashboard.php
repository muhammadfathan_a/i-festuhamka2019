<?php require('custom/_dashboard.html'); ?>
<div class="row">
    <?php $Select->dataOnlySelect($Query->JumlahDataSeminar); 
        while($rowSeminar = $Select->result -> fetch()) {
            $seminar = $rowSeminar[0];

            $Select->dataOnlySelect($Query->nJmlDataSeminar);
            while($rowSeminarBelumLunas = $Select->result -> fetch()) {
    ?>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
        <div class="card-body">
            <div class="clearfix">
            <div class="float-left">
                <i class="mdi mdi-lava-lamp text-warning icon-lg"></i>
            </div>
            <div class="float-right">
                <p class="mb-0 text-right"> Peserta Seminar </p>
                <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">0<?= $seminar; ?></h3>
                </div>
            </div>
            </div>
            <p class="text-muted mt-3 mb-0">
                <i class="mdi mdi-reload mr-1" aria-hidden="true">
                    <b> BELUM LUNAS </b> ( 0<?= $rowSeminarBelumLunas[0]; ?> orang )
                </i>
            </p>
        </div>
        </div>
    </div>
    <?php } } ?>
    <?php $Select->dataOnlySelect($Query->JumlahDataWorkshop);
        while($rowWorkshop = $Select->result -> fetch()) {
            $workshop = $rowWorkshop[0];

            $Select->dataOnlySelect($Query->nJmlDataWorkshop);
            while($rowWorkshopBelumLunas = $Select->result -> fetch()) {
    ?>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
        <div class="card-body">
            <div class="clearfix">
            <div class="float-left">
                <i class="mdi mdi-security-network text-success icon-lg"></i>
            </div>
            <div class="float-right">
                <p class="mb-0 text-right"> Peserta Workshop </p>
                <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">0<?= $workshop; ?></h3>
                </div>
            </div>
            </div>
            <p class="text-muted mt-3 mb-0">
                <i class="mdi mdi-reload mr-1" aria-hidden="true">
                    <b> BELUM LUNAS </b> ( 0<?= $rowWorkshopBelumLunas[0]; ?> orang )
                </i>
            </p>
        </div>
        </div>
    </div>
    <?php } } ?>
    <?php $Select->dataOnlySelect($Query->JumlahDataLomba); 
        while($rowLomba = $Select->result -> fetch()) {
            $lomba = $rowLomba[0];

            $Select->dataOnlySelect($Query->JumlahLombaPC);
            while($rowPC = $Select->result -> fetch()) {
                $lPC = $rowPC[0];

                $Select->dataOnlySelect($Query->JumlahLombaMA);
                while($rowMA = $Select->result -> fetch()) {
                    $lMA = $rowMA[0];
    ?>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
        <div class="card-body">
            <div class="clearfix">
            <div class="float-left">
                <i class="mdi mdi-trophy text-info icon-lg"></i>
            </div>
            <div class="float-right">
                <p class="mb-0 text-right"> Peserta Lomba </p>
                <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">0<?= $lomba; ?></h3>
                </div>
            </div>
            </div>
            <p class="text-muted mt-3 mb-0">
                <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true">
                    <b> Pro. Coding </b> <font class="ml-1"> (0<?= $rowPC[0]; ?> Team) </font>
                </i> 
            </p>
            <p class="text-muted mt-1 mb-0">
                <i class="mdi mdi-bookmark-outline mr-1" aria-hidden="true">
                    <b> Mobile A. UI/UX </b> <font class="ml-1"> (0<?= $rowMA[0]; ?> Team) </font>
                </i> 
            </p>
        </div>
        </div>
    </div>
    <?php } } } ?>
    <?php global $seminar, $workshop, $lomba; $total = $seminar + $workshop + $lomba; ?>
    <div class="col-xl-3 col-lg-3 col-md-3 col-sm-6 grid-margin stretch-card">
        <div class="card card-statistics">
        <div class="card-body">
            <div class="clearfix">
            <div class="float-left">
                <i class="mdi mdi-account text-primary icon-lg"></i>
            </div>
            <div class="float-right">
                <p class="mb-0 text-right"> Total Peserta </p>
                <div class="fluid-container">
                <h3 class="font-weight-medium text-right mb-0">0<?= $total; ?></h3>
                </div>
            </div>
            </div>
            <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-check-all mr-1" aria-hidden="true"><b> Terdaftar dan Lunas </b></i> 
            </p>
        </div>
        </div>
    </div>
    <?php global $lPC, $lMA; 

            $hargaSeminar = $seminar * 65000;
            $hargaWorkshop = $workshop * 50000;
            $hargaLPC = $lPC * 120000;
            $hargaLMA = $lMA * 100000;

            $totalRegist = $hargaSeminar + $hargaWorkshop + $hargaLPC + $hargaLMA; 
    
    ?>
    <div class="col-xl-12 col-lg-12 col-md-12 col-sm-12 grid-margin stretch-card">
        <div class="card card-statistics">
        <div class="card-body">
            <div class="clearfix">
            <div class="float-left">
                <p class="mb-0 text-left icon-md">
                    <i class="mdi mdi-cube text-danger icon-lg"></i> 
                    <font class="dana ml-1 top-0" style="color: #fff; font-weight: 100; top: -8; position: relative; font-size: 21px;"> 
                        Total Biaya Registrasi IFEST 
                    </font>
                </p>
            </div>
            <div class="float-right">
                <p class="mb-0 text-right"> Saat ini </p>
                <div class="fluid-container">
                    <h3 class="font-weight-medium text-right mb-0 duit"> Rp <?= number_format($totalRegist,2,',','.'); ?> </h3>
                </div>
            </div>
            </div>
            <p class="text-muted mt-3 mb-0">
            <i class="mdi mdi-alert-octagon mr-1" aria-hidden="true"></i> Semangat Temen-Temen :* 
            <font class="text-right"> HIMA TI UHAMKA! </font>
            </p>
        </div>
        </div>
    </div>
</div>