<?php 
    // delete seminar
    $id_peserta = $_GET['ds'];
    
    if($id_peserta) {
        
        $firstReplace = "1E8A4ZB10";
        $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

        $afterReplace1 = str_replace($firstReplace, "", $id_peserta);
        $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

        $Delete -> deletePeserta($afterReplace2, $Query->deleteSeminar);
        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENGHAPUS PESERTA!",
                "IFEST 4.0 - SEMINAR",
                "success"
            );
            window.location = "../";
        </script>';
    }

    // delete workshop
    $id_peserta = $_GET['dw'];
    
    if($id_peserta) {
        
        $firstReplace = "1E8A4ZB10";
        $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

        $afterReplace1 = str_replace($firstReplace, "", $id_peserta);
        $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

        $Delete -> deletePeserta($afterReplace2, $Query->deleteWorkshop);

        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENGHAPUS PESERTA!",
                "IFEST 4.0 - WORKSHOP",
                "success"
            );
            window.location = "../";
        </script>';
    }

    // delete team
    $id_team = $_GET['dt'];
    
    if($id_team) {
        
        $firstReplace = "1E8A4ZB10";
        $lastReplace  = "22D1131412555G1L78ZZHHAQEU";

        $afterReplace1 = str_replace($firstReplace, "", $id_team);
        $afterReplace2 = str_replace($lastReplace, "", $afterReplace1);

        $Delete -> deletePeserta($afterReplace2, $Query->deleteTeam);

        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENGHAPUS TEAM!",
                "IFEST 4.0 - COMPETITION",
                "success"
            );
            window.location = "../";
        </script>';
    }
?>