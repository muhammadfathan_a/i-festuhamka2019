<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php

require('../../../system/db_conn.php');
require('../../../system/db_query.php');

require('../../../process/function/fun_register.php');
require('../../../process/function/fun_delete.php');
require('../../../process/function/fun_select.php');

        $Register = new Register();
        $Delete = new Delete();
        $Query = new DBquery();
        $Select = new Select();

            $nama       = $_POST['nama_lengkap'];
            $instansi   = $_POST['instansi'];
            $email      = $_POST['email'];
            $telp       = $_POST['telp'];
            $status     = $_POST['status'];
            
        $Register->adminRegisterContent($nama, $instansi, $telp, $email, $status, $Query->adminAddWorkshop);
        $jmlRow = $Register->row -> rowCount();

    if($jmlRow > 0) {
        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENDAMBAHKAN PESERTA!",
                "IFEST - SEMINAR",
                "success"
            );
            window.go(-1);
        </script>';
        echo
        '<script>
            window.location = "../";
        </script>';
    }
    echo 
    '<script>   
        Swal.fire({
            type: "error",
            title:"GAGAL MENDAFTAR!",
            text: "NAMA SUDAH TERDAFTAR",
        })
    </script>';
    echo
    '<script>
        window.location = "../";
    </script>';

?>