<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php error_reporting(1);

require('../../../system/db_conn.php');
require('../../../system/db_query.php');

require('../../../process/function/fun_register.php');
require('../../../process/function/fun_delete.php');
require('../../../process/function/fun_select.php');

        $Register = new Register();
        $Delete = new Delete();
        $Query = new DBquery();
        $Select = new Select();

            $nama      = $_POST['nama_lengkap'];
            $edukasi   = $_POST['edukasi'];
            $institusi = $_POST['institusi'];
            $telp      = $_POST['telp'];
            $tgl_lahir = $_POST['tgl_lahir'];
            $id_team   = $_POST['id_team'];
            
        $Register->registerPeserta($nama, $edukasi, $institusi, $telp, $tgl_lahir, $id_team, $Query->addPeserta);
        $jmlRow = $Register->row -> rowCount();

    if($jmlRow > 0) {
        echo 
        '<script>
            Swal.fire(
                "BERHASIL MENDAMBAHKAN ANGGOTA!",
                "GOOD LUCK - PROGRESSIVE CODING",
                "success"
            );
            window.go(-1);
        </script>';
        echo
        '<script>
            window.location = "../";
        </script>';
    }

?>