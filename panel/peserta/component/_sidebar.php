<?php require('custom/_sidebar.html');
?>
<nav class="sidebar sidebar-offcanvas" id="sidebar">
<ul class="nav">
    <li class="nav-item nav-profile">
    <form method="post">
    <div class="nav-link">
    <button name="manage-team" class="manage-team">
        <div class="user-wrapper">
        <div class="profile-image">
            <img src="../assets/images/team/team1.png" alt="profile image">
        </div>
        <div class="text-wrapper">
            <p class="profile-name"><?= $_SESSION['ifest_peserta']['nama_team']; ?></p>
            <div>
            <small class="designation text-muted"><?= $_SESSION['ifest_peserta']['nama_kategori']; ?></small>
                <span class="status-indicator online"></span>
            </div>
        </div>
        </div>
    </button>
    </div>
    </form>
    </li>
    <form method="post">
    <li class="nav-item">
    <a class="nav-link" href="../">
        <i class="menu-icon mdi mdi-television"></i>
        <span class="menu-title">Dashboard
        </span>
    </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"> 
        <i class="menu-icon mdi mdi-human-greeting"></i> 
            <button class="update-anggota-btn" name="anggota"> My Team </button>
        </a>
    </li>
    <li class="nav-item">
        <a class="nav-link" href="#"> 
        <i class="menu-icon mdi mdi-book-open"></i> 
            <button class="update-project-btn" name="project"> Project </button>
        </a>
    </li>
    <li class="nav-item">
    <a class="nav-link" aria-expanded="false" aria-controls="ui-basic">
        <i class="menu-icon mdi mdi-content-copy"></i>
        <span class="menu-title">
            <button class="update-project-btn" name="guide"> Competition Guide </button>
        </span>
    </a>
    </li>
    </form>
</ul>
</nav>