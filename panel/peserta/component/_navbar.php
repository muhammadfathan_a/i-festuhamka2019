<?php require('custom/_nav.html'); ?>
<!-- partial:partials/_navbar.html -->
<nav class="navbar default-layout col-lg-12 col-12 p-0 fixed-top d-flex flex-row">
    <div class="text-center navbar-brand-wrapper d-flex align-items-top justify-content-center">
    <a class="navbar-brand brand-logo" href="../">
        <img src="../assets/images/logoV3.png" alt="logo" />
    </a>
    <a class="navbar-brand brand-logo-mini" href="../">
        <img src="../assets/images/logoV2Color.png" alt="logo" />
    </a>
    </div>
    <div class="navbar-menu-wrapper d-flex align-items-center">
    <!-- <ul class="navbar-nav navbar-nav-left header-links d-none d-md-flex">
        <li class="nav-item">
        <a href="#" class="nav-link">
            <span class="badge badge-primary ml-2"></span>
        </a>
        </li>
        <li class="nav-item">
        <a href="#" class="nav-link">
            <i class="mdi mdi-email-variant"></i> muhammadfathan23@gmail.com </a>
        </li>
        <li class="nav-item active">
        <a href="#" class="nav-link">
            <i class="mdi mdi-elevation-rise"></i> Statistik </a>
        </li>
    </ul> -->
    <ul class="navbar-nav navbar-nav-right">
        <li class="nav-item dropdown">
        <a class="nav-link count-indicator dropdown-toggle" id="notificationDropdown" href="#" data-toggle="dropdown">
            <i class="mdi mdi-bell"></i>
            <span class="count">4</span>
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown preview-list" aria-labelledby="notificationDropdown">
            <a class="dropdown-item">
            <p class="mb-0 font-weight-normal float-left">You have 4 new notifications
            </p>
            <span class="badge badge-pill badge-warning float-right">View all</span>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
                <div class="preview-icon bg-success">
                <i class="mdi mdi-alert-circle-outline mx-0"></i>
                </div>
            </div>
            <div class="preview-item-content">
                <h6 class="preview-subject font-weight-medium text-dark">Application Error</h6>
                <p class="font-weight-light small-text">
                Just now
                </p>
            </div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
                <div class="preview-icon bg-warning">
                <i class="mdi mdi-comment-text-outline mx-0"></i>
                </div>
            </div>
            <div class="preview-item-content">
                <h6 class="preview-subject font-weight-medium text-dark">Settings</h6>
                <p class="font-weight-light small-text">
                Private message
                </p>
            </div>
            </a>
            <div class="dropdown-divider"></div>
            <a class="dropdown-item preview-item">
            <div class="preview-thumbnail">
                <div class="preview-icon bg-info">
                <i class="mdi mdi-email-outline mx-0"></i>
                </div>
            </div>
            <div class="preview-item-content">
                <h6 class="preview-subject font-weight-medium text-dark">New user registration</h6>
                <p class="font-weight-light small-text">
                2 days ago
                </p>
            </div>
            </a>
        </div>
        </li>
        <li class="nav-item dropdown d-none d-xl-inline-block">
        <a class="nav-link dropdown-toggle" id="UserDropdown" href="#" data-toggle="dropdown" aria-expanded="false">
            <span class="profile-text"> Hello, <b><?= $_SESSION['ifest_peserta']['nama_team']; ?>!</b></span>
            <!-- <img class="img-xs rounded-circle" src="assets/images/faces/team1.png" alt="Profile image"> -->
        </a>
        <div class="dropdown-menu dropdown-menu-right navbar-dropdown" aria-labelledby="UserDropdown">
        <form method="post">
            <a class="dropdown-item p-0">
            </a>
            <a class="dropdown-item mt-2">
                <button name="manage-team" class="manage-btn">
                    Manage Account
                </button>    
            </a>
            <a class="dropdown-item">
                <button name="logout" class="logout-btn">
                    Sign Out
                </button>
            </a>
        </form>
        </div>
        </li>
    </ul>
    <button class="navbar-toggler navbar-toggler-right d-lg-none align-self-center" type="button" data-toggle="offcanvas">
        <span class="mdi mdi-menu"></span>
    </button>
    </div>
</nav>
<?php session_start();
    $logout = $_POST['logout'];
    
    if(isset($logout)) {
        UNSET($_SESSION['ifest_peserta']);
        echo '<script> window.location = "../../login/"; </script>';
    }
?>