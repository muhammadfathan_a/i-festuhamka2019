<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

<link rel="stylesheet" href="../assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="../assets/vendors/css/vendor.bundle.addons.css">

<link rel="stylesheet" href="../assets/css/style.css">

<link rel="shortcut icon" href="../assets/images/favicon.png" />

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<script src="../assets/js/jquery-3.4.1.min.js"></script>