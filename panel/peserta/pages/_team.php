<?php 
  require('custom/_team.html'); ?>
  <div class="anggota">
  <div class="container-scroller">
    <!-- partial -->
        <div class="content-wrapper">
          <div class="row">
            <div class="col-lg-12 stretch-card">
              <div class="card">
                <div class="card-body">
                <button class="click-profile">
                  <img src="../assets/images/team/<?= $_SESSION['ifest_peserta']['photo_profile']; ?>" alt="logo" class="photo-profile">
                </button>
                <button class="manage-team">
                  <h1 class="card-title"><?= $_SESSION['ifest_peserta']['nama_team']; ?>! 
                    <i class="mdi mdi-dots-horizontal text-dark pencil"></i>
                  </h1>
                </button>
                </div>
              </div>
            </div>
            <form method="get">
            <div class="col-12 grid-margin stretch-card" id="card-form">
                <div class="card">
                <div class="card-body">
                    <div id="tambahAnggota">
                    <div class="form-group manage">
                    <form method="post" action="process/pro_anggota.php">
                      <div class="input-group mt-2 email">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-email text-white"></i>
                          </span>
                          </div>
                          <input name="email" type="text" class="form-control" placeholder="Email" required="required"
                          value="<?= $_SESSION['ifest_peserta']['email']; ?>">
                      </div>
                      <div class="input-group mt-2 password">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-key text-white"></i>
                          </span>
                          </div>
                          <input name="password" type="password" class="form-control" placeholder="Password" required="required">
                      </div>

                    <!-- Disabled -->
                      <div class="input-group mt-2 email-disabled">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-email text-white"></i>
                          </span>
                          </div>
                          <input type="text" class="form-control" placeholder="Email" required="required"
                          value="<?= $_SESSION['ifest_peserta']['email']; ?>" disabled="disabled">
                      </div>
                      <div class="input-group mt-2 password-disabled">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-key text-white"></i>
                          </span>
                          </div>
                          <input type="password" class="form-control" placeholder="Password" required="required" disabled="disabled"
                          value="<?= $_SESSION['ifest_peserta']['password']; ?>">
                      </div>
                    <!-- End Disabled -->

                      <div class="input-group mt-2" style="display: none;">
                          <div class="input-group-prepend bg-warning">
                          <span class="input-group-text bg-transparent">
                              <i class="mdi mdi-shield text-white"></i>
                          </span>
                          </div>
                          <input name="txomad" type="number" 
                          class="form-control" placeholder="No team" 
                          value="<?= $_SESSION['ifest_peserta']['id_team']; ?>">
                      </div>
                      <div class="input-group mt-2 update">
                          <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
                          <button id="btn-tambah-anggota" name="team_submit"> UPDATE TEAM </button>
                      </div>
                    </form>
                    </div>
                    </div>
                </div>
                </div>
            </div>
            </form>
          </div>
        </div>
    <!-- page-body-wrapper ends -->
  </div>
</div>
<script>
  $('.email').hide();
  $('.password').hide();
  $('.update').hide();

  $('.manage-team').click(function() {
    $('.email-disabled').hide();
    $('.password-disabled').hide();
    $('.email').show();
    $('.password').show();
    $('.update').show();
      
  });
</script>
<?php require('process/pro_team.php'); ?>