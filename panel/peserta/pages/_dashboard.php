<?php date_default_timezone_set('Asia/Jakarta'); ?>
<div class="row dashboard">
<div class="col-lg-7 grid-margin stretch-card">
    <!--weather card-->
    <div class="card card-weather">
    <div class="card-body">
        <div class="weather-date-location">
        <h3> <?= date('l'); ?> </h3>
        <p class="text-gray">
            <span class="weather-date"><?= date('j F Y'); ?> - </span>
            <span class="weather-location"></span>
        </p>
        </div>
        <div class="weather-data d-flex">
        <div class="mr-auto">
            <h4 class="display-3">33
            <span class="symbol">&deg;</span>C</h4>
            <p> Berawan </p>
        </div>
        </div>
    </div>
    <div class="card-body p-0">
        <div class="d-flex weakly-weather">
        <div class="weakly-weather-item">
            <p class="mb-0">
            Sun
            </p>
            <i class="mdi mdi-weather-cloudy"></i>
            <p class="mb-0">
            30°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Mon
            </p>
            <i class="mdi mdi-weather-hail"></i>
            <p class="mb-0">
            31°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Tue
            </p>
            <i class="mdi mdi-weather-partlycloudy"></i>
            <p class="mb-0">
            28°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Wed
            </p>
            <i class="mdi mdi-weather-pouring"></i>
            <p class="mb-0">
            30°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Thu
            </p>
            <i class="mdi mdi-weather-pouring"></i>
            <p class="mb-0">
            29°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Fri
            </p>
            <i class="mdi mdi-weather-snowy-rainy"></i>
            <p class="mb-0">
            31°
            </p>
        </div>
        <div class="weakly-weather-item">
            <p class="mb-1">
            Sat
            </p>
            <i class="mdi mdi-weather-snowy"></i>
            <p class="mb-0">
            32°
            </p>
        </div>
        </div>
    </div>
    </div>
    <!--weather card ends-->
</div>
<div class="col-lg-5 grid-margin stretch-card">
    <div class="card">
    <div class="card-body">
        <h2 class="card-title text-primary mb-5"> Skor Penilaian </h2>
        <div class="wrapper d-flex justify-content-between">
        <div class="side-left">
            <p class="mb-2"> Aspek Pertama </p>
            <p class="display-3 mb-4 font-weight-light"> - </p>
        </div>
        <div class="side-right">
            <small class="text-muted">POINT</small>
        </div>
        </div>
        <div class="wrapper d-flex justify-content-between">
        <div class="side-left">
            <p class="mb-2"> Aspek Kedua </p>
            <p class="display-3 mb-4 font-weight-light"> - </p>
        </div>
        <div class="side-right">
            <small class="text-muted">POINT</small>
        </div>
        </div>
        <div class="wrapper d-flex justify-content-between">
        <div class="side-left">
            <p class="mb-2"> Aspek Ketiga </p>
            <p class="display-3 mb-4 font-weight-light"> - </p>
        </div>
        <div class="side-right">
            <small class="text-muted">POINT</small>
        </div>
        </div>
        <div class="wrapper d-flex justify-content-between">
        <div class="side-left">
            <p class="mb-2"> Aspek Keempat </p>
            <p class="display-3 mb-4 font-weight-light"> - </p>
        </div>
        <div class="side-right">
            <small class="text-muted">POINT</small>
        </div>
        </div>
        <div class="wrapper d-flex justify-content-between">
        <div class="side-left">
            <p class="mb-2"> Aspek Kelima </p>
            <p class="display-3 mb-4 font-weight-light"> - </p>
        </div>
        <div class="side-right">
            <small class="text-muted">POINT</small>
        </div>
        </div>
        <div class="wrapper mt-4">
        <div class="d-flex justify-content-between">
            <p class="mb-2"> POINT RATA-RATA </p>
            <p class="mb-2 text-success">??%</p>
        </div>
        <div class="progress">
            <div class="progress-bar bg-success progress-bar-striped progress-bar-animated" role="progressbar" style="width: 96%" aria-valuenow="96" aria-valuemin="0" aria-valuemax="100"></div>
        </div>
        </div>
    </div>
    </div>
</div>
</div>
</div>