<link rel="shortcut icon" type="text/icon" href="../../assets/images/faviconV3.ico">
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php session_start(); error_reporting(1);
    
    require('../../system/db_conn.php');
    require('../../system/db_query.php');

    require('../../process/function/fun_register.php');
    require('../../process/function/fun_edit.php');
    require('../../process/function/fun_delete.php');
    require('../../process/function/fun_select.php');

      $Register = new Register();
      $Edit     = new Edit();
      $Delete   = new Delete();
      $Query    = new DBquery();
      $Select   = new Select();

        $dashboard = $_POST['dashboard'];
        $anggota   = $_POST['anggota'];
        $project   = $_POST['project'];
        $guide     = $_POST['guide'];
        $manage    = $_POST['manage-team'];
      
    if(!$_SESSION['ifest_peserta'] && !$_SESSION['ifest_admin']) {
        echo '<script> window.location = "../../login/"; </script>';
    }
    include "library/_lib-css.php"; 
?>
<body>
  <div class="container-scroller">
    <?php include "component/_navbar.php"; ?>
      <div class="container-fluid page-body-wrapper">
      <?php include "component/_sidebar.php"; ?>
      <div class="main-panel">
        <div class="content-wrapper">
        <?php // include "_popup-message.php"; ?>
          <?php 

                if(isset($anggota) || $_GET['y']) {
                  echo "<title> Team | IFEST </title>";
                  include "pages/_anggota.php";
                } 
                else if(isset($project)) {
                  echo "<title> Project | IFEST </title>";
                  include "pages/_project.php";
                } 
                else if(isset($guide)) {
                  echo "<title> Guide | IFEST </title>";
                  include "pages/_guide.php";
                }
                else if(isset($manage)) {
                  echo "<title> Manage Team | IFEST </title>";
                  include "pages/_team.php";
                }
                else {
                  echo "<title> Dashboard | IFEST </title>";
                  include "pages/_dashboard.php";
                }
                
          ?>
        <!-- close of content-wrapper -->
        </div>
        <?php include "component/_footer.php"; ?>
      <!-- close of main-panel -->
      </div>
    <!-- close of container-fluid page-body-wrapper -->
    </div>
  <!-- close of container-scroller -->
  </div>
</body>
<?php include "library/_lib-js.php"; ?>
<?php include "process/pro_delete.php"; ?>
<noscript> 
  <style>
    * {
      display: none;
    }
  </style>
</noscript>