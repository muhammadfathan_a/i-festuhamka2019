<p class="card-description">
Edit ur team member
</p>
<form method="post">
<div id="editAnggota">
<div class="input-group">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-account text-white"></i>
    </span>
    </div>
    <input name="nama_lengkap" type="text"
    class="form-control" placeholder="Nama lengkap" value="<?= $row['nama_peserta'] ?>">
</div>
<div class="input-group mt-2">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-school text-white"></i>
    </span>
    </div>
    <select name="edukasi" class="form-control">
    <option> Sekolah Menengah Pertama </option>
    <option> Sekolah Menengah Atas </option>
    <option> Mahasiswa </option>
    <option> Profesional </option>
    </select>
</div>
<div class="input-group mt-2">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-city text-white"></i>
    </span>
    </div>
    <input name="institusi" type="text" 
    class="form-control" placeholder="Institusi" value="<?= $row['institusi'] ?>">
</div>
<div class="input-group mt-2">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-phone text-white"></i>
    </span>
    </div>
    <input name="telp" type="number" 
    class="form-control" placeholder="Nomor telepon" value="<?= $row['no_telp'] ?>">
</div>
<div class="input-group mt-2">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-calendar text-white"></i>
    </span>
    </div>
    <input name="tgl_lahir" type="date" 
    class="form-control" placeholder="Tanggal lahir" value="<?= $row['tgl_lahir'] ?>">
</div>
<div class="input-group mt-2" style="display: none;">
    <div class="input-group-prepend bg-warning">
    <span class="input-group-text bg-transparent">
        <i class="mdi mdi-shield text-white"></i>
    </span>
    </div>
    <input  name="id_team"
            type="number" 
            class="form-control" 
            placeholder="No team" 
            value="<?= $_SESSION['ifest_peserta']['id_team']; ?>" 
    >
</div>
<div class="input-group mt-2">
    <!-- <button name="anggota-submit" class="btn-submit bg-primary"> SUBMIT </button> -->
    <button class="btn-tambah-anggota" name="anggota-edit"> UPDATE ANGGOTA </button>
</div>
</div>
</form>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
<?php error_reporting(1);

        global $afterReplace2;

            $id        = $afterReplace2;
            $nama      = $_POST['nama_lengkap'];
            $edukasi   = $_POST['edukasi'];
            $institusi = $_POST['institusi'];
            $telp      = $_POST['telp'];
            $tgl_lahir = $_POST['tgl_lahir'];
            
        if(isset($_POST['anggota-edit'])) {
            $Edit->editPeserta($nama, $edukasi, $institusi, $telp, $tgl_lahir, $id, $Query->editPeserta);
            
            echo 
            '<script>
                Swal.fire(
                    "BERHASIL UBAH ANGGOTA!",
                    "GOOD LUCK - PROGRESSIVE CODING",
                    "success"
                );
                window.location = "../";
            </script>';        
        }
  

?>