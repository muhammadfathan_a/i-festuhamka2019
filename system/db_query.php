<?php 
    class DBquery {

        // Login Query
        public $SelectPeserta       = "SELECT t_team.*, t_kategori.* FROM t_team 
                                        INNER JOIN t_kategori ON t_kategori.id_kategori = t_team.id_kategori 
                                        WHERE t_team.email=? AND t_team.password=?";
                                        
        public $SelectAdmin         = "SELECT * FROM t_admin WHERE username=? AND password=MD5(?)"; 
        

        // Select Entry Query
        public $DataPeserta         = "SELECT t_peserta.*, t_team.* FROM t_peserta
                                        INNER JOIN t_team ON t_peserta.id_team = t_team.id_team
                                        WHERE t_team.id_team = ?";

        public $countDataPeserta    = "SELECT t_peserta.* FROM t_peserta
                                        INNER JOIN t_team ON t_peserta.id_team = t_team.id_team
                                        WHERE t_team.id_team=?";

        public $onlyDataPC          = "SELECT t_peserta.*, t_team.*, t_kategori.* FROM t_peserta
                                        INNER JOIN t_team ON t_peserta.id_team = t_team.id_team 
                                        INNER JOIN t_kategori ON t_team.id_kategori = t_kategori.id_kategori
                                        WHERE t_kategori.nama_kategori = 'Pro. Coding' 
                                        ORDER BY t_team.nama_team DESC";

        public $onlyDataMA          = "SELECT t_peserta.*, t_team.*, t_kategori.* FROM t_peserta
                                        INNER JOIN t_team ON t_peserta.id_team = t_team.id_team 
                                        INNER JOIN t_kategori ON t_team.id_kategori = t_kategori.id_kategori
                                        WHERE t_kategori.nama_kategori = 'Mobile App UI / UX'
                                        ORDER BY t_team.nama_team DESC";

        public $onlyDataTeam        = "SELECT t_team.*, t_kategori.* FROM t_team 
                                        INNER JOIN t_kategori ON t_team.id_kategori = t_kategori.id_kategori
                                        ORDER BY t_team.status DESC, t_team.active DESC";   
                                        

        public $onlyDataTeamById    = "SELECT t_team.*, t_kategori.* FROM t_team 
                                        INNER JOIN t_kategori ON t_team.id_kategori = t_kategori.id_kategori
                                        WHERE t_team.id_team = ?
                                        ORDER BY t_team.status DESC, t_team.active DESC";     
                                        
        public $CountPesertaTeam    = "SELECT t_peserta.*, t_team.*, t_kategori.* FROM t_team 
                                        INNER JOIN t_kategori ON t_team.id_kategori = t_kategori.id_kategori
                                        INNER JOIN t_peserta WHERE t_peserta.id_team=? ORDER BY t_kategori.nama_kategori DESC";    

        public $DataSeminarLunas    = "SELECT * FROM t_seminar WHERE status='LUNAS' ORDER BY status DESC";

        public $DataSeminarBlmLunas = "SELECT * FROM t_seminar WHERE status='BELUM LUNAS' ORDER BY status DESC";

        public $DataWorkshopLunas   = "SELECT * FROM t_workshop WHERE status='LUNAS' ORDER BY status DESC";

        public $DataWorkshopBlmLunas= "SELECT * FROM t_workshop WHERE status='BELUM LUNAS' ORDER BY status DESC";

        public $DataSeminarDoang    = "SELECT * FROM t_seminar WHERE id_peserta = ?";

        public $DataWorkshopDoang   = "SELECT * FROM t_workshop WHERE id_peserta = ?";

        public $DataPesertaDoang    = "SELECT * FROM t_peserta WHERE id_peserta = ?";

        public $JumlahDataSeminar   = "SELECT COUNT(id_peserta) FROM t_seminar WHERE status = 'LUNAS'";
        
        public $nJmlDataSeminar    = "SELECT COUNT(id_peserta) FROM t_seminar WHERE status = 'BELUM LUNAS'";

        public $JumlahDataWorkshop  = "SELECT COUNT(id_peserta) FROM t_workshop WHERE status = 'LUNAS'";

        public $nJmlDataWorkshop   = "SELECT COUNT(id_peserta) FROM t_workshop WHERE status = 'BELUM LUNAS'";

        public $JumlahDataLomba     = "SELECT COUNT(id_peserta) FROM t_peserta";

        public $PesertaByTeam       = "SELECT COUNT(id_peserta) FROM t_peserta WHERE id_team = ?";
        
        public $JumlahLombaPC       = "SELECT COUNT(id_team) FROM t_team WHERE id_kategori = 1";

        public $JumlahLombaMA       = "SELECT COUNT(id_team) FROM t_team WHERE id_kategori = 2";

        public $selectMailer        = "SELECT * FROM t_team WHERE email = ?";

        public $dataEmail           = "SELECT DISTINCT t_seminar.email, t_workshop.email, t_team.email FROM t_seminar, t_workshop, t_team";


        // Insert Entry Query
        public $addWorkshop         = "INSERT INTO t_workshop 
                                        (nama, instansi, email, telp, waktu_daftar)
                                        VALUES 
                                        (?, ?, ?, ?, NOW())";

        public $addSeminar          = "INSERT INTO t_seminar 
                                        (nama, instansi, email, telp, waktu_daftar)
                                        VALUES 
                                        (?, ?, ?, ?, NOW())";

        public $adminAddWorkshop    = "INSERT INTO t_workshop 
                                        (nama, instansi, email, telp, status, waktu_daftar)
                                        VALUES 
                                        (?, ?, ?, ?, ?, NOW())";

        public $adminAddSeminar     = "INSERT INTO t_seminar 
                                        (nama, instansi, email, telp, status, waktu_daftar)
                                        VALUES 
                                        (?, ?, ?, ?, ?, NOW())";

        public $addPC               = "INSERT INTO t_team
                                        (nama_team, id_kategori, email, password)
                                        VALUES
                                        (?, '1', ?, ?)";

        public $addMA               = "INSERT INTO t_team
                                        (nama_team, id_kategori, email, password)
                                        VALUES
                                        (?, '2', ?, ?)";

        public $addPeserta          = "INSERT INTO t_peserta
                                        (nama_peserta, edukasi, institusi, no_telp, tgl_lahir, id_team) 
                                        VALUES
                                        (?, ?, ?, ?, ?, ?)";


        // Delete Entry Query
        public $deletePeserta       = "DELETE FROM t_peserta WHERE id_peserta=?";

        public $deleteSeminar       = "DELETE FROM t_seminar WHERE id_peserta=?";

        public $deleteWorkshop      = "DELETE FROM t_workshop WHERE id_peserta=?";

        public $deleteTeam          = "DELETE FROM t_team WHERE id_team=?";


        // Update Entry Query
        public $editPeserta         = "UPDATE t_peserta SET nama_peserta = ? , edukasi = ? , 
                                        institusi = ? , no_telp = ? , tgl_lahir = ? WHERE id_peserta = ?";

        public $editTeam            = "UPDATE t_team SET email = ? , password = MD5(?) WHERE id_team = ?";

        public $editSeminar         = "UPDATE t_seminar SET nama = ? , instansi = ? , 
                                        email = ? , telp = ? , status = ? , waktu_daftar = NOW() WHERE id_peserta = ?";

        public $editWorkshop        = "UPDATE t_workshop SET nama=? , instansi=? , 
                                        email=? , telp=? , status=? , waktu_daftar = NOW() WHERE id_peserta = ?";

        public $adminTeamEdit       = "UPDATE t_team set nama_team=? , email=? , status=? , active=? WHERE id_team=?";

    }