<head>
    <?php
        require("lib/head.php"); 
    ?>
</head>
<!-- <body 
    oncontextmenu='return false;' 
    onkeydown='return false;' 
    onmousedown='return false;' > -->
<body>
    <?php
        require("component/01_nav.php");
    ?>
    <main>
        <br>
        <?php  
            require("component/03_about.php"); 
            require("component/08_contact.php"); 
            require("component/09_banner.php"); 
            require("component/10_footer.php"); 
        ?>
    </main>
    <?php 
        require("lib/foot.php"); 
    ?>
</body>