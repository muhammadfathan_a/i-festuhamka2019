<?php require("custom/08_contact.html"); ?>
<div class="content" id="contact">
    <div class="btn-close-modal" data-close="modal">
        <span class="icon ma-cross"></span>
    </div>
    <div class="container-contact">
        <div class="contact-caption">
            <div class="contact-content">
                <h2 class="c-white animated fadeInLeft">Our Contact :</h2>
                <p class="animated fadeInLeft">
                    <span class="icon ma-map"></span>&nbsp; 
                    Jl. Tanah Merdeka No. 06, Kp. Rambutan, Pasar Rebo, Jakarta Timur 
                </p>
                <p class="animated fadeInLeft">
                    <span class="icon ma-phone"></span>&nbsp; 
                    0896 4933 8089
                </p>
                <p class="animated fadeInLeft">
                    <span class="icon ma-envelope"></span>&nbsp; 
                    info@himatiftuhamka.co.id
                </p>
                <p class="animated fadeInLeft">
                    <span class="icon ma-internet"></span>&nbsp; 
                    himatiftuhamka.co.id
                </p>
            </div>
            <br>
        </div>
        <div class="flat-browser-container">
            <div class="flat-browser">
                <div class="flat-header">
                    <div class="flat-menu">
                        <span class="icon ma-menu"></span>
                    </div>
                    <div class="flat-caption">
                        <div class="caption">Maps</div>
                    </div>
                </div>
                <div class="flat-body">
                    <div id="maps"></div>
                </div>
            </div>
        </div>
    </div>
</div>