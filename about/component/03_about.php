<?php require('custom/03_about.html'); ?>
<div class="about-ifest">
    <div class="container container-left" id="about">
        <div class="row">
            <div class="cell-td4">
                <div class="img-place about">
                    <img src="../assets/images/PaSugema.png" class="img-about animated fadeIn">
                </div>
            </div>
            <div class="cell-td8">
                <div class="caption-content">
                    <h1 class="about-title animated fadeInLeft"> ABOUT IFEST </h1>
                    <h1 class="sub-about-title animated fadeInLeft">
                        <font class="biggest"> THE BIGGEST </font> 
                        IT FESTIVAL AT UHAMKA 
                    </h1>
                    <p class="content animated fadeInRight"><b style="font-weight: 900;"> I-FEST </b> merupakan acara tahunan yang diadakan HIMA TI UHAMKA bertujuan sebagai wadah untuk mengembangkan wawasan dan kemampuan IT untuk bisa diimplementasikan ke kehidupan sehari-hari. Selain itu juga demi meningkatkan kualitas SDM untuk bersaing khususnya dalam bidang IT. Tahun ini merupakan tahun ke empat kami mengadakan acara ini, dari <b> I-FEST </b> sebelumnya yang sudah sukses dilaksanakan. </p>
                </div>
            </div>
        </div>
    </div>
</div>