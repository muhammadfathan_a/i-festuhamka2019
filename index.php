<?php session_start(); error_reporting(1); 

    $galeri = $_POST['galeri-btn'];
    if(isset($galeri)) {

?>
<head>
    <?php
        echo "<title> Gallery | IFEST </title>";
        require("lib/head.php");
    ?>
</head>
<body>
<?php
        require("component/01_nav.php");
    ?>
<main>
    <br>
    <?php
        require("component/11_gallery.php"); 
        require("component/09_banner.php"); 
        require("component/10_footer.php"); 
    ?>
</main>
    <?php
        require("lib/foot.php");
    ?>
</body>
<?php 
    } else {
?>
<head>
    <?php
        require("lib/head.php"); 
    ?>
</head>
<body 
    "oncontextmenu='return false;'"
    "onkeydown='return false;'" 
    "onmousedown='return false;'" >
<body>
    <?php
        require("component/01_nav.php"); 
        require("component/02_header.php");
    ?>
    <main>
        <br>
        <?php  
            require("component/03_about.php"); 
            require("component/12_alur.php"); 
            require("component/05_content.php"); 
            require("component/04_competition.php"); 
            // require("component/06_itcare.php");
            // require("component/07_support.php");    
            require("component/08_contact.php");
            require("component/09_banner.php"); 
            require("component/10_footer.php"); 
        ?>
    </main>
    <?php
        require("lib/foot.php"); }
    ?>
</body>
<noscript> 
  <style>
    * {
      display: none;
    }
  </style>
</noscript>