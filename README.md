# I-FEST UHAMKA 2019

**THE BIGGEST IT FESTIVAL AT UHAMKA**

I-FEST merupakan acara tahunan yang diadakan HIMA TI UHAMKA bertujuan sebagai wadah untuk mengembangkan wawasan dan kemampuan IT untuk bisa diimplementasikan ke kehidupan sehari-hari. Selain itu juga demi meningkatkan kualitas SDM untuk bersaing khususnya dalam bidang IT. Tahun ini merupakan tahun ke empat kami mengadakan acara ini, dari I-FEST sebelumnya yang sudah sukses dilaksanakan.
