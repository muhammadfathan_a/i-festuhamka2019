	// Navigation
jQuery(document).ready(function() {
	var menuNav = $('.navmenu');

	$('.nav-toggle').click(function() {
		$('.navmenu').slideToggle('fast');
	});
	$(window).resize(function() {
        var w = $(window).width();
        if(w > 768 && menuNav.is(":hidden")) {
            menuNav.removeAttr('style');
        }
    });
});
	// Particle Ground
jQuery(document).ready(function() {
	$('#img-place').particleground({
		dotColor: '#fff',
		lineColor: 'transparent',
		particleRadius: '3',
		density: '8000',
		directionY: 'up',
		parallax: false
	});
});
	// Maps
function initMap() {
    var uhamkaTeknik = {lat: -6.3040792, lng: 106.8714642};
    var map = new google.maps.Map(document.getElementById('maps'), {
        zoom: 17,
        center: uhamkaTeknik
    });
    var marker = new google.maps.Marker({
        position: uhamkaTeknik,
        map: map
    });
}
	// Modal Contact
jQuery(document).ready(function() {
	$('.full-page').click(function() {
		$('.contact-caption').fadeOut();
		$('#contact').addClass('active');
		$('.container-contact').addClass('active');
		$('.flat-browser').addClass('active');
		$('.btn-close-modal').addClass('active');
		$('html,body').css('overflow', 'hidden');
	});
	$('.btn-close-modal').click(function() {
		$('.contact-caption').fadeIn();
		$('#contact').removeClass('active');
		$('.container-contact').removeClass('active');
		$('.flat-browser').removeClass('active');
		$(this).removeClass('active');
		$('html,body').removeAttr('style');
	});
});
