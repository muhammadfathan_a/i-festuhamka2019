$(document).ready(function() {

		$('#calendar').fullCalendar({
			defaultDate: '2017-02-12',
			editable: true,
			eventLimit: true, // allow "more" link when too many events
			events: [
				{
					title: 'Registrasi dan Pengumpulan Project',
					start: '2017-03-10',
                    end: '2017-03-30'
				},
				{
					title: 'Deadline Project',
					start: '2017-03-30'
				},
				{
					title: 'Penyisihan 5 Finalis',
					start: '2017-03-31'
				},
				{
					title: 'Technical Meeting',
					start: '2017-04-01'
				},
				{
					title: 'Grand Final dan Opening',
					start: '2017-04-02'
				},
				{
					title: 'Pengumuman 3 Pemenang',
					start: '2017-04-08'
				},
				{
					title: 'Expo Karya Pemenang',
					start: '2017-04-08'
				}
			]
		});
		
	});