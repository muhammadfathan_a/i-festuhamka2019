<?php
    ini_set( 'display_errors', 1 );   
    error_reporting( E_ALL );    
    $from = "akhtarfath@ifestuhamka.com";    

    $to = "muhammadfathan23@gmail.com";     

    $subject = "I-FEST UHAMKA 2019";

    $message = 
    '<div class="col-md-12">
    <div class="message" style="width: 600px; margin: 25px auto; border: 2px solid #74a1d8;">
        <div class="head-message" style="margin: 0 auto;">
            <h2 style="margin: 0 auto; text-align: center; background: #74a1d8; padding: 20px; color: #fff;> 
                INVITATION <br> I-FEST UHAMKA 2019 
            </h2>
        </div>
        <div class="body-message" style="color: #fff; background-image: url(../assets/images/mail.png); background-size: 100%; background-position: center;">
            <p style="padding-top: 25px; text-align: center; font-size: 16px;">
                <b> Muhammad Fathan Aulia <br> Universitas Muhammadiyah Prof. Dr. HAMKA </b>
            </p>
            <p style="padding-top: 25px; text-align: center;">
                PESERTA - WORKSHOP <br> <i>"Improve The Experience With Data Science"</i>
            </p>
            <p style="margin: 0 auto; text-align: center; padding-bottom: 30px;">
                <b> Hari / Tanggal : Jumat, 05 Juli 2019 </b> 
                <br>
                <b> Tempat : Fakultas Teknik UHAMKA </b>
            </p>
        </div>
    </div>';   

    $headers = "From:" . $from;    
    mail($to, $subject, $message, $headers);    
    
    echo "Pesan email sudah terkirim.";
?>