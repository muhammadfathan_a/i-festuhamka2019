<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="viewport" content="width=device-width, initial-scale=1.0">

<link rel="shortcut icon" type="text/icon" href="assets/images/faviconV3.ico">
<link rel="stylesheet" type="text/css" href="assets/landing/css/macode-icons.css">
<link rel="stylesheet" type="text/css" href="assets/landing/css/macode.css">
<link rel="stylesheet" type="text/css" href="assets/landing/css/macode-responsive.css">
<link rel="stylesheet" type="text/css" href="assets/landing/css/animate.css">
<link rel="stylesheet" type="text/css" href="assets/landing/css/style.css">
<link rel="stylesheet" type="text/css" href="assets/css/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.8.2/css/all.css" 
integrity="sha384-oS3vJWv+0UjzBfQzYUhtDYW+Pj2yciDJxpsK1OYPAYjqT085Qq/1cq5FLXAZQ7Ay" crossorigin="anonymous">
<!-- Compiled and minified CSS -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/1.0.0/css/materialize.min.css">
<link rel="stylesheet" href="https://unpkg.com/aos@next/dist/aos.css" />

<title>
	Welcome | IFEST
</title>

<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8"></script>
