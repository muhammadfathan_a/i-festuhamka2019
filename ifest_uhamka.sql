-- phpMyAdmin SQL Dump
-- version 4.8.3
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Sep 04, 2019 at 05:56 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 7.2.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `ifest_uhamka`
--

-- --------------------------------------------------------

--
-- Table structure for table `t_admin`
--

CREATE TABLE `t_admin` (
  `id_admin` int(11) NOT NULL,
  `nama_admin` varchar(255) NOT NULL,
  `username` varchar(255) NOT NULL,
  `password` char(32) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_admin`
--

INSERT INTO `t_admin` (`id_admin`, `nama_admin`, `username`, `password`) VALUES
(1, 'ADMIN', 'ifestuhamka', '21232f297a57a5a743894a0e4a801fc3');

-- --------------------------------------------------------

--
-- Table structure for table `t_berkas`
--

CREATE TABLE `t_berkas` (
  `id_berkas` int(11) NOT NULL,
  `alamat_berkas` varchar(255) NOT NULL,
  `link_berkas` varchar(255) NOT NULL,
  `id_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_kategori`
--

CREATE TABLE `t_kategori` (
  `id_kategori` int(11) NOT NULL,
  `nama_kategori` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_kategori`
--

INSERT INTO `t_kategori` (`id_kategori`, `nama_kategori`) VALUES
(1, 'Pro. Coding'),
(2, 'Mobile App UI / UX');

-- --------------------------------------------------------

--
-- Table structure for table `t_pesan`
--

CREATE TABLE `t_pesan` (
  `id_pesan` varchar(255) NOT NULL,
  `isi_pesan` text NOT NULL,
  `id_team` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_peserta`
--

CREATE TABLE `t_peserta` (
  `id_peserta` int(11) NOT NULL,
  `nama_peserta` varchar(255) NOT NULL,
  `edukasi` enum('Sekolah Menengah Pertama','Sekolah Menengah Atas','Mahasiswa','Profesional') NOT NULL,
  `institusi` varchar(255) NOT NULL,
  `no_telp` varchar(15) NOT NULL,
  `tgl_lahir` date NOT NULL,
  `id_team` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `t_seminar`
--

CREATE TABLE `t_seminar` (
  `id_peserta` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `telp` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('LUNAS','BELUM LUNAS') NOT NULL DEFAULT 'BELUM LUNAS',
  `waktu_daftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_seminar`
--

INSERT INTO `t_seminar` (`id_peserta`, `nama`, `instansi`, `telp`, `email`, `status`, `waktu_daftar`) VALUES
(5, 'LINDA SARI DEWI', 'STMIK NUSAMANDIRI', '087889222256', 'LINDA.LRW@BSI.AC.ID', 'LUNAS', '2019-06-21'),
(6, 'Rinawati', 'STMIK NUSA MANDIRI', '081806043155', 'rinatelaga@gmail.com', 'LUNAS', '2019-06-21'),
(7, 'ERENE GERNARIA SIHOMBING, M.KOM', 'STMIK NUSA MANDIRI JAKARTA', '081288758588', 'erene.egs@nusamandir.ac.id', 'LUNAS', '2019-06-21'),
(9, 'Yumi Novita Dewi, M.Kom', 'STMIK Nusa Mandiri Jakarta', '081384769679', 'yuminovitadewi@gmail.com', 'BELUM LUNAS', '2019-06-29'),
(11, 'WAHYU  INDRARTI', 'UNIV. BSI JAKARTA', '081284893958', 'wahyuindrarti03@gmail.com', 'BELUM LUNAS', '2019-06-21'),
(12, 'Ichsan Wahyu Saputro', 'Universitas Bina Sarana Informatika Jakarta', '085319244861', 'ichsan.3112@gmail.com', 'BELUM LUNAS', '2019-06-21'),
(14, 'BAMBANG JUNADI M.KOM', 'UNIVERSITAS BSI', '08159411840', 'BAMBANG.JUNADI@GMAIL.COM', 'BELUM LUNAS', '2019-06-21'),
(15, 'Sumaiya ', 'FKIP UHAMKA', '082122238411', 'seatar123@gmail.com', 'BELUM LUNAS', '2019-06-21'),
(17, 'Muhammad Azhar Hidayatullah Hendra', 'Universitas Gunadarma', '087782097341', 'azharmuhammad040@gmail.com', 'LUNAS', '2019-06-22'),
(18, 'Rizqi Agung Permana', 'Sigma', '081286536550', 'rizqiagung@gmail.com', 'LUNAS', '2019-06-22'),
(19, 'Reza Pangestu', 'Universitas Gunadarma', '081318878143', 'rezapangestu854@gmail.com', 'LUNAS', '2019-06-22'),
(20, 'Ichroman Raditya', 'LINE Indonesia', '085216065838', 'ichromanraditya.duwila@gmail.com', 'BELUM LUNAS', '2019-06-21'),
(21, 'Ayu Tanjung', 'Fakultas teknik uhamka', '089618399311', 'ayutanjung16@gmail.com', 'BELUM LUNAS', '2019-06-21'),
(22, 'ISMI NUR AZIZ', 'UNIVERSITAS INDRAPRASTA PGRI TEKNIK INFORMATIKA', '082123894020', 'isminurazizz@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(25, 'Titi Silfia ', 'Stikom Cki ', '081293070492 ', 'fiviasilviatiti1101@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(27, 'Vika vitaloka pramansah', 'STIKOM Cipta Karya Informatika', '089616925969', 'vikavp0@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(28, 'eka pasha', 'suryaduta teknologi', '089601008153', 'pashaeka@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(29, 'Khidir Zahid', 'UIKA Bogor', '089606730041', 'khidirzahid@gmail.com', 'BELUM LUNAS', '2019-06-24'),
(30, 'Sony Agustiansyah', 'STIKOM CKI', '081386414410', 'sonyagustiansyah@gmail.com', 'BELUM LUNAS', '2019-06-25'),
(31, 'Alghifari Arief Noerwangsa', 'Universitas Muhammadiyah Prof. Dr. Hamka', '085883871977', 'farinoerwangsa@gmail.com', 'BELUM LUNAS', '2019-06-26'),
(32, 'Luqman Hakim', 'FT Uhamka', '085280151137', 'ripperz_55@yahoo.com', 'BELUM LUNAS', '2019-06-26'),
(33, 'Dini Hadi Pratiwi', 'Unis Tangerang', '081717747155', 'dinihadi10@gmail.com', 'BELUM LUNAS', '2019-06-26'),
(34, 'Muhammad Fathan', 'CUK', '08978329974', 'muhammadfathan23@gmail.com', 'BELUM LUNAS', '2019-07-01');

-- --------------------------------------------------------

--
-- Table structure for table `t_team`
--

CREATE TABLE `t_team` (
  `id_team` int(11) NOT NULL,
  `nama_team` varchar(255) NOT NULL,
  `id_kategori` int(11) DEFAULT NULL,
  `email` varchar(255) NOT NULL,
  `password` char(32) NOT NULL,
  `status` enum('LUNAS','BELUM LUNAS') DEFAULT 'BELUM LUNAS',
  `active` enum('AKTIF','BELUM AKTIF') DEFAULT 'BELUM AKTIF'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_team`
--

INSERT INTO `t_team` (`id_team`, `nama_team`, `id_kategori`, `email`, `password`, `status`, `active`) VALUES
(36, 'SESat', 1, 'fazri121014@gmail.com', 'sesat19', 'LUNAS', 'AKTIF');

-- --------------------------------------------------------

--
-- Table structure for table `t_workshop`
--

CREATE TABLE `t_workshop` (
  `id_peserta` int(11) NOT NULL,
  `nama` varchar(255) NOT NULL,
  `instansi` varchar(255) NOT NULL,
  `telp` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `status` enum('LUNAS','BELUM LUNAS') NOT NULL DEFAULT 'BELUM LUNAS',
  `waktu_daftar` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `t_workshop`
--

INSERT INTO `t_workshop` (`id_peserta`, `nama`, `instansi`, `telp`, `email`, `status`, `waktu_daftar`) VALUES
(12, 'Rizky Zein Adam', 'FT UHAMKA', '0895330123049', 'rizkyzeinadam@gmail.com', 'LUNAS', '2019-06-22'),
(14, 'ISMI NUR AZIZ', 'UNIVERSITAS INDRAPRASTA PGRI TEKNIK INFORMATIKA', '082123894020', 'isminurazizz@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(19, 'Syukron Fajri Afiatanto', 'UHAMKA', '089520836552', 'syukron.fajri97@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(21, 'Annur Fuad', 'TI UHAMKA', '089604236160', 'annurfuad83@gmail.com', 'LUNAS', '2019-06-25'),
(22, 'muhamad sopiyan', 'universitas Nasional', '089501480176', 'sofyanm625@gmail.com', 'BELUM LUNAS', '2019-06-23'),
(24, 'Hermariyanti Kusumadewi', 'Universitas Indraprasta PGRI', '081289493291', 'khermariyanti@gmail.com', 'LUNAS', '2019-06-24'),
(26, 'Kusrini', 'Uhamka', '082398174828', 'kusrininuhdar2016@gmail.com', 'BELUM LUNAS', '2019-06-24'),
(27, 'Irawatty huath', 'Universitas Muhammadiyah prof. Dr. Hamka', '085299526802', 'irawatty23@gmail.com', 'BELUM LUNAS', '2019-06-24'),
(28, 'marwati', 'Sekolah Pascasarjana Universitas Muhammadiyah Prof. DR. Hamka', '082374039438', 'marwati2615@gmail.com', 'BELUM LUNAS', '2019-06-24'),
(29, 'Luluk nurjanah', 'Pascasarjana UHAMKA', '082373153498', 'lulunurjannah962@yahoo.com', 'BELUM LUNAS', '2019-06-25'),
(30, 'Lailana Deviani', 'Sekolah Pascasarjana UHAMKA', '082175272018', 'lailanadeviani@gmail.com', 'BELUM LUNAS', '2019-06-25'),
(32, 'Yang Yuniarti Romza', 'Pascasarjana Uhamka', '082144469142', 'yayang.yyr29@gmail.com', 'BELUM LUNAS', '2019-06-25'),
(33, 'iin Sutisna', 'sekolah pacsasarja universitas nuhammadiyah Prof. DR. hamka', '085716697158', 'iinsutisna@ymail.com', 'BELUM LUNAS', '2019-06-25'),
(34, 'Alghifari Arief Noerwangsa', 'Universitas Muhammadiyah Prof. Dr. Hamka', '085883871977', 'farinoerwangsa@gmail.com', 'LUNAS', '2019-06-27'),
(35, 'meri liandani', 'sekolah pascasarjana universitas muhammadiyah prof. Dr Hamka', '081928119002', 'meriliandani18@gmail.com', 'BELUM LUNAS', '2019-06-26'),
(37, 'Marudut Bernadtua Simanjuntak, M.Pd', 'Universitas Indraprasta PGRI', '+628953OO67684', 'bernadmarudut@gmail.com', 'BELUM LUNAS', '2019-06-26'),
(38, 'Titi Silfia', 'mahasiswa STIKOM CKI', '081293070492', 'fiviasilviatiti1101@gmail.com', 'BELUM LUNAS', '2019-06-27'),
(39, 'Anas Mabruki', 'Fakultas Teknik UHAMKA', '08978664207', 'ansmbrk7@gmail.com', 'LUNAS', '2019-06-27'),
(40, 'Rizki Gunawan', 'Fakultas Teknik UHAMKA', '085210328579', 'rizkigunawan7@gmail.com', 'LUNAS', '2019-06-27'),
(41, 'Shandi Pratama Shaleh', 'Fakultas Teknik UHAMKA', '083814992179', 'shandipshaleh39@yahoo.com', 'BELUM LUNAS', '2019-06-27');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `t_admin`
--
ALTER TABLE `t_admin`
  ADD PRIMARY KEY (`id_admin`);

--
-- Indexes for table `t_berkas`
--
ALTER TABLE `t_berkas`
  ADD PRIMARY KEY (`id_berkas`),
  ADD KEY `t_berkas_ibfk_1` (`id_team`);

--
-- Indexes for table `t_kategori`
--
ALTER TABLE `t_kategori`
  ADD PRIMARY KEY (`id_kategori`);

--
-- Indexes for table `t_pesan`
--
ALTER TABLE `t_pesan`
  ADD PRIMARY KEY (`id_pesan`);

--
-- Indexes for table `t_peserta`
--
ALTER TABLE `t_peserta`
  ADD PRIMARY KEY (`id_peserta`),
  ADD KEY `t_peserta_ibfk_1` (`id_team`);

--
-- Indexes for table `t_seminar`
--
ALTER TABLE `t_seminar`
  ADD PRIMARY KEY (`id_peserta`),
  ADD UNIQUE KEY `nama` (`nama`),
  ADD UNIQUE KEY `telp` (`telp`,`email`);

--
-- Indexes for table `t_team`
--
ALTER TABLE `t_team`
  ADD PRIMARY KEY (`id_team`),
  ADD UNIQUE KEY `email` (`email`),
  ADD KEY `t_team_ibfk_1` (`id_kategori`);

--
-- Indexes for table `t_workshop`
--
ALTER TABLE `t_workshop`
  ADD PRIMARY KEY (`id_peserta`),
  ADD UNIQUE KEY `nama` (`nama`),
  ADD UNIQUE KEY `telp` (`telp`,`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `t_admin`
--
ALTER TABLE `t_admin`
  MODIFY `id_admin` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `t_berkas`
--
ALTER TABLE `t_berkas`
  MODIFY `id_berkas` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_kategori`
--
ALTER TABLE `t_kategori`
  MODIFY `id_kategori` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `t_peserta`
--
ALTER TABLE `t_peserta`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `t_seminar`
--
ALTER TABLE `t_seminar`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=35;

--
-- AUTO_INCREMENT for table `t_team`
--
ALTER TABLE `t_team`
  MODIFY `id_team` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=37;

--
-- AUTO_INCREMENT for table `t_workshop`
--
ALTER TABLE `t_workshop`
  MODIFY `id_peserta` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=42;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `t_berkas`
--
ALTER TABLE `t_berkas`
  ADD CONSTRAINT `t_berkas_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `t_team` (`id_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_peserta`
--
ALTER TABLE `t_peserta`
  ADD CONSTRAINT `t_peserta_ibfk_1` FOREIGN KEY (`id_team`) REFERENCES `t_team` (`id_team`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `t_team`
--
ALTER TABLE `t_team`
  ADD CONSTRAINT `t_team_ibfk_1` FOREIGN KEY (`id_kategori`) REFERENCES `t_kategori` (`id_kategori`) ON DELETE CASCADE ON UPDATE CASCADE;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
